import React, {useEffect, useState} from "react";
import {Link, useHistory} from "react-router-dom"
import {
    ButtonIcon,
    Header,
    Hr,
    Icons,
    Menu,
    MenuTab,
    Name,
    ProfileImage,
    ProfileInfo,
    ProfileText,
    Selected,
    StyledPopup,
    SubjectSection,
    Username
} from "./LeftSideMenu.styles"
import {default as http} from "../../util/Http.js"
import {keywords} from "../../config/KeyWords";
import {connect} from "react-redux";
import {EN} from "../../Dictionaries/EN";
import {RU} from "../../Dictionaries/RU";
import setLanguageCreator from "../../store/actionCreators/setLanguageCreator";
import updateProfileCreator from "../../store/actionCreators/updateProfileCreator";
import forceUpdateCreator from "../../store/actionCreators/forceUpdateCreator";
import Notifications from "../notifications/Notifications";
import Search from '../search/Search'
import {hasRole} from "../../util/ProfileUtils";

const mapDispatchToProps = dispatch => ({
    setLanguage: (dictionary) => dispatch(setLanguageCreator(dictionary)),
    updateProfile: (profile) => dispatch(updateProfileCreator(profile)),
    forceUpdate: () => dispatch(forceUpdateCreator())
});

const mapStateToProps = state => ({
    dictionary: state.dictionary,
    profile: state.profile,
    dummy: state.updateDummyFlag,
});

const MenuButtonComponent = connect(mapStateToProps, mapDispatchToProps)(MenuButton);


function LeftSideMenu(props) {
    const [searchTrigger, setSearchTrigger] = useState(false)
    const closeModal = () => setSearchTrigger(false);

    const [ready, setReady] = useState(false);

    const [notifications, setNotifications] = useState(false);

    const [hasNewNotifications, setNewNotifications] = useState(false);

    const updateProfile = props.updateProfile;

    useEffect(() => {
        http.get(keywords.USERS + keywords.ME).then((response) => {
            setReady(true);
            updateProfile(response.data)
        })
    }, [updateProfile]);

    useEffect(() => {

    }, [props.match])

    const path = window.location.href;

    const newNotificationsHandler = () => {
        setNewNotifications(true);
    }

    return <Header>
        <Notifications open={notifications} closeOnDocumentClick={false} onClose={() => setNotifications(false)}
                       modal ready={ready} newNotificationsHandler={newNotificationsHandler} history={props.history}/>
        <Link to={"/profile/" + props.profile.id}>
            <div onClick={props.forceUpdate}>
                <ProfileInfo>
                    <ProfileImage src={props.profile.imageId ?
                        keywords.IMAGES + "?id=" + props.profile.imageId : "/img/profile.png"}
                                  alt={"Profile"}/>
                    <ProfileText>
                        <Username> {props.profile.username} </Username>
                        <Name> {props.profile.firstName + " " + props.profile.lastName} </Name>
                    </ProfileText>
                </ProfileInfo>
                <Hr/>
            </div>
        </Link>
        <Menu>
            <MenuTab>
                <MenuButtonComponent href="/main" text={props.dictionary.HOME}
                                     isSelected={path.indexOf("/main") !== -1}/>
            </MenuTab>
            <SubjectMenu path={path} text={props.dictionary.SUBJECTS}
                         isAdmin={hasRole(props.profile, keywords.ROLE_ADMIN)}/>
            <MenuTab>
                <MenuButtonComponent href="/logout" text={props.dictionary.LOG_OUT}
                                     isSelected={false}/>
            </MenuTab>
        </Menu>
        <Icons>
            <Link to={"/messenger"}><ButtonIcon defaultUrl={"/img/messages.png"}
                                                hoverUrl={"/img/messages_blue.png"}/></Link>
            <ButtonIcon defaultUrl={"/img/search.png"} hoverUrl={"/img/search.png"}
                        onClick={() => {
                            setSearchTrigger(searchTrigger => !searchTrigger)
                            setNotifications(false);
                        }}/>
            <StyledPopup open={searchTrigger} closeOnDocumentClick={false} modal onClose={closeModal}>
                {close => <Search dictionary={props.dictionary} close={close}/>}
            </StyledPopup>

            {hasNewNotifications ?
                <ButtonIcon defaultUrl={"/img/notifications_new.png"} hoverUrl={"/img/notifications_new_blue.png"}
                            onClick={(e) => {
                                e.stopPropagation();
                                setNewNotifications(false);
                                setSearchTrigger(false);
                                setNotifications(notifications => !notifications);
                            }}/> :
                <ButtonIcon defaultUrl={"/img/notifications.png"} hoverUrl={"/img/notifications_blue.png"}
                            onClick={(e) => {
                                e.stopPropagation();
                                setSearchTrigger(false);
                                setNotifications(notifications => !notifications);
                            }}/>}
            <ButtonIcon defaultUrl={"/img/translate.png"} hoverUrl={"/img/translate_blue.png"}
                        onClick={() => props.setLanguage(props.dictionary === EN ? RU : EN)}/>
        </Icons>
    </Header>
}

function MenuButton({isSelected, forceUpdate, text, href}) {
    if (isSelected) {
        return (
            <ul><Link to={href}><Selected>{text}</Selected></Link></ul>
        );
    } else {
        return (
            <ul onClick={() => forceUpdate()}><Link to={href}>{text}</Link></ul>
        );
    }
}


function SubjectMenu({path, text, isAdmin}) {
    const SubjectButton = ({isSelected, hover, setHover}) => {

        const hoverHandler = (status) => {
            setHover(status);
        }

        return (
            <div style={{cursor: isAdmin ? "pinter" : "default"}}>
                {isSelected || hover ?
                    <Selected
                        onMouseEnter={() => hoverHandler(true)}>
                        {isAdmin ? <Link to={"/admin/subjectsManagement"}>{text} &darr;</Link> :
                            <span>{text} &darr;</span>}
                    </Selected> :
                    <div onMouseEnter={() => hoverHandler(true)}>{isAdmin ?
                        <Link to={"/admin/subjectsManagement"}>{text} &darr;</Link> : <span>{text} &darr;</span>}</div>}
            </div>
        );
    }


    const SubjectsList = (
        {
            path, setHover
        }
    ) => {
        return (
            <SubjectSection style={{display: "block"}} onMouseLeave={() => setHover(false)}>
                <SubjectsArrayView path={path}/>
            </SubjectSection>
        );
    }

    const [hover, setHover] = useState(false);

    const SubjectsArrayView = (
        {
            path
        }
    ) => {

        const [subjects, setSubjects] = useState([
            {
                id: 0,
                name: "",
            },
        ]);

        useEffect(() => {
            http.get(keywords.SUBJECTS).then((response) => {
                setSubjects(response.data);

            })
        }, [])

        if (subjects.length === 0) {
            return (
                <div>
                    No subjects :-(
                </div>
            )
        }
        return subjects.map((subject) =>
            <div key={subject.id}>
                <MenuButtonComponent href={"/subject/id" + subject.id} text={subject.name}
                                     isSelected={path.indexOf("/subject/id" + subject.id) !== -1}/>
            </div>
        );
    }

    return (
        <MenuTab onMouseLeave={() => setHover(false)}>
            <SubjectButton hover={hover} isSelected={path.indexOf("/subject") !== -1} setHover={setHover}/>
            {hover ? <SubjectsList path={path} opened={hover} setHover={setHover}/> : <div/>}
        </MenuTab>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(LeftSideMenu);