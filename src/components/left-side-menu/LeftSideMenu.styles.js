import styled from 'styled-components';
import Popup from 'reactjs-popup';

export const Header = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	top: 0;
	left: 0;
	width: 18.22vw;
	min-height: 100vh;
	background: #2e2e2e;
	float: left;
	z-index: 9999;
`;

export const ProfileInfo = styled.div`
	align-items: center;
	display: flex;
	flex-flow: wrap column;
	padding: 2.6vw 2.6vw 0;
	margin-top: 0;
	margin-left: 0;
	height: 11.5vh;
`;

export const ProfileText = styled.div``;

export const ProfileImage = styled.img`
	height: 4.2vw;
	width: 4.2vw;
	border: #ffffff;
	border-radius: 100%;
`;

export const Username = styled.h3`
	display: block;
	position: relative;
	top: 0.54vh;
	font-family: Arial, serif;
	font-size: 2vh;
	color: #4a90e2;
	line-break: strict;
`;

export const Name = styled(Username)`
	display: block;
	position: relative;
	top: 1.8vh;
	font-size: 1.5vh;
	color: #f77a52;
`;

export const Hr = styled.hr`
	position: relative;
	width: calc(100% - 2vw * 2);
`;

export const Selected = styled.h3`
	color: #ffffff;
`;

export const Menu = styled.div`
	position: relative;
	padding: 0 2.6vw;
	flex: 1 0 auto;
`;

export const MenuTab = styled.div`
	position: relative;
	top: 3.4vh;
	padding: 1.55vh;
	font-family: Arial, serif;
	font-size: 2vh;
	color: #9d959d;
`;
export const SubjectSection = styled.section`
	padding: 1.55vh;
`;

export const Icons = styled.div`
	display: flex;
	flex-flow: row wrap;
	position: relative;
	left: 1.65vw;
	width: calc(100% - 1.65vw * 2);
	bottom: 2.95vw;
	flex: 0 0 auto;
`;

export const Icon = styled.img`
	position: relative;
	width: 2.05vw;
	margin: 0 1.65vw 0 0;
`;

export const ButtonIcon = styled(Icon)`
	cursor: pointer;
	content: url(${(props) => props.defaultUrl});
	&:hover {
		content: url(${(props) => props.hoverUrl});
	}
`;
export const StyledPopup = styled(Popup)`
	&-overlay {
		backdrop-filter: blur(5px);
	}
	&-content {
	}
`;
