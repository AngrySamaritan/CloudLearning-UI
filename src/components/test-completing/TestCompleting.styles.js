import styled from "styled-components";
import {DefaultForm, DefaultH3, DefaultH4} from "../Common.styles";
import {Checkbox, Radiobutton, Text} from "../inputs";

export const MainForm = styled(DefaultForm)`
  cursor: default;
  position: fixed;
  display: flex;
  flex-flow: column nowrap;
  top: 50px;
  left: 21%;
  width: 76%;
  height: 90%;
  background-color: #fff;
  opacity: 1;
  border-radius: 2vh;
  -webkit-border-radius: 2vh;
`

export const Clocks = styled.div`
  z-index: 5;
  color: #F77A52;
  font-family: Arial, serif;
  font-size: 1.6vmin;

  position: absolute;
  left: 1.5em;
  top: 1.5em;
  width: 5em;
  height: 5em;
  border: 2px solid #4a90e2;
  border-radius: 50%;
  line-height: 5em;
  text-align: center;
`

export const SaveButton = styled(Clocks)`
  right: 1.5em;
  cursor: pointer;
  left: auto;

  &:hover {
    border-color: #F77A52;
  }
`

export const QuestionForm = styled.div`
  display: flex;
  flex-flow: row nowrap;
  margin-left: auto;
  margin-right: auto;
  width: 95%;
  margin-top: 2vh;
  overflow-y: auto;
  height: 20%;
  border: #2e2e2e solid 1px;
`

export const QuestionImage = styled.img`
  height: 90%;
  margin-right: 5%;
  margin-left: 5%;
`

export const TestName = styled(DefaultH3)`
  color: black;
  font-family: Arial, serif;

  font-weight: bold;
  display: flex;
  flex-flow: row;
  margin-left: 2.5%;
  align-items: flex-end;
  justify-content: center;
  min-height: 12vh;
  width: 95%;
  border-bottom: #4a90e2 2px solid;
  padding-bottom: 1vh;
`

export const QuestionText = styled(DefaultH4)`
  white-space: pre-line;
  margin: 1vh 1.5vw;
  font-size: 2.5vh;
  word-wrap: break-word;
`

export const QuestionLabel = styled(DefaultH4)`
  font-size: 2.5vh;
  white-space: pre;
  float: left;
  color: black;
  font-weight: bold;
`

export const AnswersList = styled.div`
  margin-top: 2vh;
  flex: 1;
  color: black;
`

export const AnswerRow = styled.div`
  margin-left: calc(2.5% + 1.5vw);
  display: flex;
  height: 6%;
  margin-bottom: 2%;
  width: 80%;
  overflow-y: auto;
`

export const AnswerText = styled(DefaultH4)`
`

export const SingleSelect = styled(Radiobutton)`
`

export const MultipleSelect = styled(Checkbox)`
`

export const Writing = styled(Text)`
`

export const Buttons = styled.div`
  display: flex;
  flex-flow: row nowrap;
  margin-bottom: 2vh;
`

export const PrevButton = styled.button`
  width: calc(150vw / 19.2);
  height: calc(40vh / 10.8);
  font-family: Arial, serif;
  font-size: 2vmin;
  background: transparent;
  color: #454545;
  border: 2px solid #4a90e2;
  cursor: pointer;
  margin-right: auto;
  margin-left: 2.5vw;
  margin-top: 5vh;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;


  &:hover {
    border-color: #F77A52;
    color: #F77A52;
  }
`

export const NextButton = styled(PrevButton)`
  margin-left: auto;
  margin-right: 2.5vw;
`