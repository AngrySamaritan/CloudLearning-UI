import React, {useEffect, useState} from "react"
import {connect} from "react-redux";
import {
    AnswerRow,
    AnswersList,
    AnswerText, Buttons,
    Clocks,
    MainForm, MultipleSelect, NextButton, PrevButton,
    QuestionForm, QuestionImage,
    QuestionLabel,
    QuestionText,
    SaveButton,
    SingleSelect,
    TestName, Writing
} from "./TestCompleting.styles";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import {QuestionTypes} from "../../config/QuestionsTypes";
import {useHistory} from "react-router-dom";
import {OverlayImg, PreviewOverlay} from "../test-edit/TestEdit.styles";

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
})

const TestCompleting = ({match: {params: {id}}, dictionary}) => {

    const [test, setTest] = useState(
        {
            name: dictionary.LOADING,
            questions: [
                {
                    id: -1,
                    text: dictionary.LOADING,
                    type: QuestionTypes.SINGLE_SELECT_QUESTION,
                    answers: [
                        {
                            id: -1,
                            text: dictionary.LOADING,
                        }
                    ]
                }
            ]
        }
    );

    const [answers, setAnswers] = useState({});

    const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);

    const [time, setTime] = useState({
        minutes: 0,
        seconds: 0,
    });

    useEffect(() => {
        http.get(keywords.TESTS + "/" + id).then(
            response => {
                setTest(response.data);
                setCurrentQuestionIndex(0);
                setTime(time => ({...time, minutes: response.data.duration}));

            }
        ).catch(
            error => console.error(error)
        )
    }, [id]);

    useEffect(() => {
        setInterval(() => {
            setTime(time => {
                console.log(time);
                if (time.seconds === 0) {
                    time.seconds = 59;
                    time.minutes = time.minutes - 1;
                } else {
                    time.seconds = time.seconds - 1;
                }
                return {...time};
            })
        }, 1000);
    }, []);


    const AnswerRowResolver = (answer, index, answers) => {
        const id = answer.id;
        switch (test.questions[currentQuestionIndex].type) {
            case QuestionTypes.WRITING_QUESTION:
                return <Writing value={answers[currentQuestionIndex]} onChange={e => handleAnswerChange(e, id)}/>
            case QuestionTypes.SINGLE_SELECT_QUESTION:
                return <AnswerRow key={index} onClick={(e) => handleAnswerChange(e, id)}>
                    <SingleSelect checked={answers[test.questions[currentQuestionIndex].id] === id}/>
                    <AnswerText>{index + 1 + ". " + answer.text}</AnswerText>
                </AnswerRow>

            case QuestionTypes.MULTIPLE_SELECT_QUESTION:
                return <AnswerRow key={index} onClick={(e) => handleAnswerChange(e, id)}>
                    <MultipleSelect
                        checked={answer[currentQuestionIndex] && answers[currentQuestionIndex].includes(id)}/>
                    <AnswerText>{index + 1 + ". " + answer.text}</AnswerText>
                </AnswerRow>
            default:
                console.error("Unknown question type " + test.questions[currentQuestionIndex].type);
                return <div/>
        }
    }

    const handleAnswerChange = (e, id) => {
        switch (test.questions[currentQuestionIndex].type) {
            case QuestionTypes.WRITING_QUESTION:
                setAnswers(answers => {
                    answers[currentQuestionIndex][test.questions[currentQuestionIndex].id] = e.target.value;
                    return answers;
                });
                break;
            case QuestionTypes.SINGLE_SELECT_QUESTION:
                console.log("ALL RIGHT")
                setAnswers(answers => {
                        answers[test.questions[currentQuestionIndex].id] = id;
                        return answers;
                    }
                );
                break;
            case QuestionTypes.MULTIPLE_SELECT_QUESTION:
                setAnswers(answers => {
                    const qid = test.questions[currentQuestionIndex].id;
                    if (answers[qid]) {
                        if (!answers[qid].includes(id)) {
                            answers[qid].push(id);
                        } else {
                            answers[qid] = answers[qid].filter(aid => aid !== id);
                        }
                    } else {
                        answers[qid] = [id];
                    }
                    return answers;
                });
                break;
            default:
                console.error("Unknown question type " + test.questions[currentQuestionIndex].type);
        }
    };

    const history = useHistory();

    const submit = () => {
        http.post(keywords.TESTS + "/" + test.id + keywords.CHECK, answers).then(
            response => {
                history.push("/test/result/id" + response.data)
            }
        )
    };

    const [preview, setPreview] = useState(false);

    const handlePreview = () => {
        setPreview(true);
    }

    return <MainForm>
        <PreviewOverlay open={preview} onClose={() => setPreview(false)}>
            <OverlayImg
                src={test.questions[currentQuestionIndex].imageId ? keywords.IMAGES + "?id=" + test.questions[currentQuestionIndex].imageId : null}
                alt={"Error"}/>
        </PreviewOverlay>
        <Clocks>
            {(time.minutes < 10 ? "0" + time.minutes : time.minutes) + ":"
            + (time.seconds < 10 ? "0" + time.seconds : time.seconds)}
        </Clocks>
        <SaveButton onClick={submit}>
            {dictionary.SAVE_TEST}
        </SaveButton>
        <TestName>
            {test.name}
        </TestName>
        <QuestionForm>
            <QuestionText>
                <QuestionLabel>
                    {(currentQuestionIndex + 1) + ".  "}
                </QuestionLabel>
                {test.questions[currentQuestionIndex].text}
            </QuestionText>
            <div style={{flex: 1}}/>
            {test.questions[currentQuestionIndex].imageId ?
                <QuestionImage onClick={handlePreview} src={keywords.IMAGES + "?id=" + test.questions[currentQuestionIndex].imageId}/>
                : <div/>}
        </QuestionForm>
        <AnswersList>
            {test.questions[currentQuestionIndex].answers.map(
                (answer, index) => {
                    return (
                        AnswerRowResolver(answer, index, answers)
                    )
                }
            )}
        </AnswersList>
        <Buttons>
            {currentQuestionIndex !== 0 ? <PrevButton onClick={() => setCurrentQuestionIndex(current => current - 1)}>
                {dictionary.PREV}
            </PrevButton> : <div/>}
            <div style={{flex: 1}}/>
            {currentQuestionIndex !== (test.questions.length - 1) ?
                <NextButton onClick={() => setCurrentQuestionIndex(current => current + 1)}>
                    {dictionary.NEXT}
                </NextButton> : <div/>}
        </Buttons>
    </MainForm>
}

export default connect(mapStateToProps)(TestCompleting);