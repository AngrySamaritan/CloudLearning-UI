import styled from 'styled-components';

export const GroupCard = styled.div`
	position: fixed;
	left: 20vw;
	top: 3.55vh;
	height: 92.9vh;
	width: 33vw;
	background: #ffffff;
	display: flex;
	flex-direction: column;
	border-radius: 20px;
`;

export const CardHeader = styled.div`
  height: 7.7%;
  background: #2e2e2e;
  width: 100%;
  border-radius: 20px 20px 0 0;
`;

export const CardH2 = styled.h2`
	position: relative;
	left: 10.6%;
	top: 28%;
	font-family: Arial, serif;
	font-style: normal;
	font-weight: normal;
	font-size: 32px;
	line-height: 32px;
	display: flex;
	align-items: center;
	color: #ffffff;
`;

export const CardBody = styled.div`
	left: auto;
	width: 100%;
	height: 90.7%;
	overflow-y: auto;
	overflow-x: hidden;
`;

export const GroupMember = styled.div`
	position: relative;
	left: 3%;
	height: 4.3%;
	font-family: Arial, serif;
	font-style: normal;
	font-weight: normal;
	font-size: 32px;
	line-height: 32px;
	display: flex;
	align-items: center;
	color: #2e2e2e;
	margin-top: 3%;
	&:active {
		background: #4a90e2;
	}
`;

export const CardFooter = styled.div`
	background: #2e2e2e;
	height: 1.6%;
	border-radius: 0px 0px 20px 20px;
`;

export const MemberCard = styled.div`
	position: fixed;
	left: 55vw;
	top: 3.55vh;
	height: 92.9vh;
	width: 22vw;
	background: #ffffff;
	display: flex;
	flex-direction: column;
	border-radius: 20px;
`;

export const MemberCardBody = styled.div`
	width: 100%;
	height: 90.7%;
	overflow-y: auto;
	overflow-x: hidden;
`;

export const Break = styled.div`
	flex-basis: 100%;
	height: 0;
`;

export const ProfileImage = styled.img`
    border-radius: 50%;
	position: relative;
	margin-top: 3%;
	left: 9%;
	height: 18vw;
	width: 18vw;
`;

export const CardLabel = styled.h6`
	font-family: Arial, serif;
	font-style: normal;
	font-weight: normal;

	/* identical to box height */
	height: 16px;
	display: flex;
	align-items: center;

	color: #f77a52;
`;

export const CardContent = styled.div`
	margin-top: 10%;
	width: 90%;
	margin-left: 5%;
`;

export const CardH1 = styled.div`
	position: relative;
	margin-top: 3%;
	font-family: Arial, serif;
	font-style: normal;
	font-weight: normal;
	font-size: 36px;
	line-height: 36px;
	display: flex;
	align-items: center;
	color: #2e2e2e;
`;
