import React, {useEffect, useState} from 'react';
import {keywords} from "../../config/KeyWords";
import http from "../../util/Http";

import {
    CardBody,
    CardContent,
    CardFooter,
    CardH1,
    CardH2,
    CardHeader,
    CardLabel,
    GroupCard,
    GroupMember,
    MemberCard,
    MemberCardBody,
    ProfileImage
} from './Group.styles';


const Group = (props) => {
    const [groupMembers, setGroupMembers] = useState([1,1])
    const [groupNum, setGroupNum] = useState('')
    const [selectedMember, setSelectedMember] = useState({})

    useEffect(() => {
        http.get(keywords.GROUPS +  "/" + props.match.params.id).then((response) =>{
            let groupUsers = response.data;
            setGroupMembers(groupUsers.users)
            setGroupNum(groupUsers.code)
        })
    }, [props.match.params.id]);

    useEffect(() => {
        if (groupMembers[0]) {
            http.get(keywords.USERS+'/' + groupMembers[0].id).then((response) => {
                let selectedUser = response.data;
                setSelectedMember(selectedUser)
                console.log(selectedUser)
            })
        }

    }, [groupMembers]);
    
    function MemberProfileCard() {
        return (
            <MemberCard>
                <CardHeader>
                    <CardH2>
                        {selectedMember.firstName + ' ' + selectedMember.lastName}
                    </CardH2>
                </CardHeader>
                <MemberCardBody>
                    <ProfileImage src={selectedMember.imageId ? keywords.IMAGES + "?id=" + selectedMember.imageId :
                        '/img/profileImage.png' } alt="Profile"/>
                    <CardContent>
                        <CardLabel>Name</CardLabel>
                        <CardH1>
                            {selectedMember.firstName + ' ' + selectedMember.lastName}
                        </CardH1>
                    </CardContent>
                </MemberCardBody>
                <CardFooter>

                </CardFooter>
            </MemberCard>
        )
    }

    function ListOfMembers() {
        function ClickMaster(id) {
            console.log(id)
            http.get(keywords.USERS+'/' + id).then((response) => {
                let selectedUser = response.data;
                setSelectedMember(selectedUser)
            })
           }
        return groupMembers.map(
            (user) => {
                return (
                    <GroupMember onChange={(e) => {ClickMaster(e.target.value)}}>
                        <input style={{display: "none"}} type='radio' value={user.id} id={user.id}/>
                        <label for={user.id}>{user.firstName + " " + user.lastName}</label>
                    </GroupMember>
                    
                )
            }
        )
    }
    function CardOfGroup() {
        return (
            <GroupCard>
                <CardHeader>
                    <CardH2>
                        {groupNum}
                    </CardH2>
                </CardHeader>
                <CardBody>
                    <ListOfMembers/>
                </CardBody>
                <CardFooter />
            </GroupCard>
        )
    }

    return (
        <div>
            <CardOfGroup/>
            {selectedMember.length !== 0 ? MemberProfileCard() : console.log('np')}
        </div>
    );
}

export default Group;
