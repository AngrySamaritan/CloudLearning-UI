import React, {useEffect, useRef, useState} from "react";
import {
    AddSubjectButton,
    CancelButton,
    Header,
    HeaderText,
    Label,
    List,
    MainFrame,
    NewTeacherContainer,
    NewTeacherInput,
    Overlay,
    PopupButtons,
    PopupContent,
    PopupHeader,
    PopupInput,
    SaveButton,
    SearchResult,
    SearchResults,
    SearchTeacherName,
    SearchTeacherUsername,
    SubjectRow,
    SubjectsContent,
    SubjectsFooter,
    SubjectText,
    SubjectToolbox,
    TeacherName,
    TeacherRow,
    TeachersFooter,
    TeachersForm,
    TeachersToolButton,
    ToolButton
} from "./SubjectManagement.styles";
import {connect} from "react-redux";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import {NotificationManager} from "react-notifications";

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
});

const notificationTimeout = 2000;

const SubjectManagement = ({dictionary, ...props}) => {

    const [subjects, setSubjects] = useState([]);

    const [selectedIndex, setSelectedIndex] = useState(0);

    const [popupOpen, setPopupOpen] = useState(false);

    const popupTitle = useRef(dictionary.ADD_SUBJECT);

    const save = useRef(() => alert("ERROR!"));

    const [updating, setUpdating] = useState(false);

    useEffect(() => {
        http.get(keywords.SUBJECTS).then(
            response => {
                setSubjects(response.data);
            }
        ).catch(
            (error) => {
                console.log(error);
            }
        )
        setUpdating(false);
    }, [updating]);

    const initial = useRef("");


    const removeSubject = (e, id) => {
        e.stopPropagation();
        http.delete(keywords.SUBJECTS + "/" + id).then(
            () => {
                NotificationManager.success(dictionary.SUBJECT_REMOVED_SUCCESSFULLY, dictionary.SUCCESS, notificationTimeout);
                setUpdating(true);
            }
        ).catch(
            () => {
                NotificationManager.error(dictionary.SUBJECT_REMOVED_ERROR, dictionary.ERROR, notificationTimeout);
            }
        )
    }

    const renameSubjectClicked = (e, {name, id}) => {
        e.stopPropagation();
        initial.current = name;
        popupTitle.current = dictionary.CHANGE_NAME;
        save.current = (text) => {
            if (text && text !== '') {
                http.patch(keywords.SUBJECTS + "/" + id, text).then(
                    () => {
                        NotificationManager.success(dictionary.SUBJECT_RENAMED_SUCCESSFULLY, dictionary.SUCCESS, notificationTimeout);
                        setPopupOpen(false);
                        setUpdating(true);
                        initial.current = "";
                    }
                ).catch(
                    () => {
                        NotificationManager.error(dictionary.SUBJECT_RENAMED_ERROR, dictionary.ERROR, notificationTimeout);
                        initial.current = "";
                    }
                )
            }
        }
        setPopupOpen(true);
    }

    const addSubject = (e) => {
        e.stopPropagation();
        popupTitle.current = dictionary.ADD_SUBJECT;
        save.current = (text) => {
            if (text && text !== '') {
                http.put(keywords.SUBJECTS, text).then(
                    () => {
                        NotificationManager.success(dictionary.SUBJECT_CREATED_SUCCESSFULLY, dictionary.SUCCESS, notificationTimeout);
                        initial.current = "";
                        setPopupOpen(false);
                        setUpdating(true);
                    }
                ).catch(
                    () => {
                        NotificationManager.error(dictionary.SUBJECT_CREATED_ERROR, dictionary.ERROR, notificationTimeout);
                        initial.current = "";
                    }
                )
            }
        }
        setPopupOpen(true);
    }

    function deleteModer(deletedId) {
        http.patch(keywords.SUBJECTS + "/" + subjects[selectedIndex].id + keywords.USERS,
            subjects[selectedIndex].teachers.map(t => t.id).filter(id => id !== deletedId)).then(
            () => {
                NotificationManager.success(dictionary.TEACHER_REMOVED_SUCCESSFULLY, dictionary.SUCCESS, notificationTimeout);
                setUpdating(true);
            }
        ).catch(
            () => {
                NotificationManager.error(dictionary.TEACHER_REMOVED_ERROR, dictionary.ERROR, notificationTimeout);
            }
        )
    }

    function addModer(addedId) {
        http.patch(keywords.SUBJECTS + "/" + subjects[selectedIndex].id + keywords.USERS,
            [...subjects[selectedIndex].teachers.map(t => t.id), addedId]).then(
            () => {
                NotificationManager.success(dictionary.TEACHER_ADDED_SUCCESSFULLY, dictionary.SUCCESS, notificationTimeout);
                setUpdating(true);
            }
        ).catch(
            () => {
                NotificationManager.error(dictionary.TEACHER_ADDED_ERROR, dictionary.ERROR, notificationTimeout);
            }
        )
    }

    return (
        <MainFrame>
            <SubjectsContent>
                <SubjectNamePopup open={popupOpen} title={popupTitle.current} dictionary={dictionary}
                                  onSave={save.current}
                                  initial={initial.current} onClose={() => setPopupOpen(false)}/>
                <Header>
                    <HeaderText>
                        {dictionary.SUBJECTS}
                    </HeaderText>
                </Header>
                <List>
                    {subjects.map((subject, index) =>
                        <SubjectRow key={subject.id} active={index === selectedIndex}
                                    onClick={() => setSelectedIndex(index)}>
                            <SubjectText title={subject.name}>{subject.name}</SubjectText>
                            <SubjectToolbox>
                                <ToolButton defaultUrl={"/img/go_to_subject.png"}
                                            hoverUrl={"/img/go_to_subject_hover.png"}
                                            onClick={() => props.history.push("/subject/id" + subject.id)}/>
                                <ToolButton
                                    defaultUrl={"/img/rename.png"}
                                    hoverUrl={"/img/rename_hover.png"}
                                    onClick={(e) => renameSubjectClicked(e, subject)}/>
                                <ToolButton defaultUrl={"/img/delete.png"}
                                            hoverUrl={"/img/delete2.png"}
                                            onClick={(e) => removeSubject(e, subject.id)}/>
                            </SubjectToolbox>
                        </SubjectRow>
                    )}
                </List>
                <SubjectsFooter>
                    <AddSubjectButton onClick={addSubject}>{dictionary.ADD_SUBJECT}</AddSubjectButton>
                </SubjectsFooter>
            </SubjectsContent>
            <TeachersForm>
                <Header>
                    <HeaderText>
                        {dictionary.TEACHERS}
                    </HeaderText>
                </Header>
                <List>
                    {subjects[selectedIndex] ? subjects[selectedIndex].teachers.map(
                        (teacher, index) =>
                            <TeacherRow key={index}>
                                <TeacherName>
                                    {teacher.firstName + " " + teacher.lastName}
                                </TeacherName>
                                <div style={{flex: 1}}/>
                                <TeachersToolButton onClick={() => deleteModer(teacher.id)}>&mdash;</TeachersToolButton>
                            </TeacherRow>
                    ) : <div/>}
                    {subjects[selectedIndex] ?
                        <TeacherAddingRow dictionary={dictionary}
                                          submit={addModer} teachers={subjects[selectedIndex].teachers}/> : <div/>}
                </List>
                <TeachersFooter/>
            </TeachersForm>
        </MainFrame>
    )
}

const TeacherAddingRow = ({dictionary, submit, teachers}) => {

    const [searchValue, setSearchValue] = useState("");
    const [ready, setReady] = useState(false);
    const [results, setResults] = useState([]);
    const [searchFocus, setSearchFocus] = useState(false);
    const [error, setError] = useState(false);
    const [selectedResult, setSelectedResult] = useState(0);

    const addId = useRef(null);
    useRef(null);
    useEffect(() => {
        setError(false);
        if (searchValue !== "") {
            http.get(keywords.USERS + keywords.SEARCH + "?query=" + searchValue).then(
                response => {
                    setResults(response.data.filter(teacher => !teachers.map(t => t.id).includes(teacher.id)));
                    setSelectedResult(selectedResult => selectedResult > response.data.length - 1 ?
                        response.data.length - 1 : selectedResult < 0 ? 0 : selectedResult);
                }
            );
        } else {
            setResults([]);
        }
    }, [searchValue, teachers]);


    function readyToAdd(index) {
        setSearchValue(results[index].firstName + " " + results[index].lastName);
        addId.current = results[index].id;
        setReady(true);
    }


    function post() {
        if (ready) {
            submit(addId.current)
            setSearchFocus(false);
            setSearchValue("");
            setReady(false);
        } else {
            setError(true);
        }
    }

    function handleInputKeydown(e) {
        if (!e.altKey) {
            switch (e.key) {
                case "Enter":
                    e.preventDefault();
                    if (!ready) {
                        if (results.length > 0) {
                            readyToAdd(selectedResult);
                        } else {
                            setError(true);
                        }
                    } else {
                        post();
                    }
                    break;
                case "ArrowDown":
                    e.preventDefault();
                    setSelectedResult(selectedResult => selectedResult === results.length - 1 ? 0 : selectedResult + 1);
                    break;
                case "ArrowUp":
                    e.preventDefault();
                    setSelectedResult(selectedResult => selectedResult === 0 ? results.length - 1 : selectedResult - 1);
                    break;
                default:
                    break;
            }
        }
    }

    return (
        <div>
            <SubjectRow>
                <NewTeacherContainer>
                    <NewTeacherInput ready={ready} value={searchValue}
                                     error={error}
                                     onChange={e => {
                                         setSearchValue(e.target.value);
                                         setReady(false);
                                         setError(false);
                                         setSearchFocus(true);
                                     }}
                                     onKeyDown={(e) => {
                                         handleInputKeydown(e);
                                     }}
                                     onFocus={() => {
                                         setSearchFocus(true);
                                         setError(false);
                                     }}
                                     onBlur={() => setTimeout(() => setSearchFocus(false), 500)}/>
                    {searchFocus ?
                        <SearchResults>
                            {results.length !== 0 ? results.map(
                                (result, index) =>
                                    <SearchResult selected={selectedResult === index} key={index}
                                                  onClick={() => readyToAdd(index)}>
                                        <SearchTeacherName>{`${result.firstName} ${result.lastName}`}</SearchTeacherName>
                                        <div style={{flex: 1}}/>
                                        <SearchTeacherUsername>{`(${result.username})`}</SearchTeacherUsername>
                                    </SearchResult>
                                ) :
                                !ready ? <Label>
                                    {searchValue === "" ? dictionary.START_TYPING : dictionary.NOFOUND}...
                                </Label> : <div/>}
                        </SearchResults> :
                        <div/>}
                </NewTeacherContainer>
                <TeachersToolButton onClick={post}>+</TeachersToolButton>
            </SubjectRow>
        </div>
    )
}

const SubjectNamePopup = ({onSave, title, open, dictionary, initial, onClose}) => {

    const [value, setValue] = useState(initial);

    return (
        <Overlay onClose={onClose} open={open} closeOnDocumentClick={false}>
            {close => <PopupContent>
                <PopupHeader>
                    <HeaderText>
                        {title}
                    </HeaderText>
                </PopupHeader>
                <PopupInput value={value} onChange={e => setValue(e.target.value)}/>
                <PopupButtons>
                    <SaveButton onClick={() => {
                        onSave(value);
                        setValue("");
                    }}>
                        {dictionary.OK}
                    </SaveButton>
                    <CancelButton onClick={() => {
                        setValue("");
                        close();
                    }}>
                        {dictionary.CANCEL}
                    </CancelButton>
                </PopupButtons>
            </PopupContent>}
        </Overlay>
    )
}

export default connect(mapStateToProps)(SubjectManagement);