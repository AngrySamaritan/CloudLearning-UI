import styled from "styled-components";
import {DefaultFooter, DefaultForm, DefaultH3, DefaultH4, DefaultHeader, DefaultTextButton} from "../Common.styles";
import Popup from "reactjs-popup";
import {Text} from "../inputs";

export const MainFrame = styled.div`
  position: fixed;
  display: flex;
  flex-flow: row nowrap;
  height: calc(975vh / 10.8);
  width: calc(1488vw / 19.2);
  margin-left: calc(400vw / 19.2);
  margin-top: calc(52vw / 19.2);
`

export const SubjectsContent = styled(DefaultForm)`
  position: relative;
  flex-flow: column nowrap;
  width: calc(450vw / 19.2);
  height: 100%;
  margin-right: calc(69vw / 19.2);
`

export const Header = styled(DefaultHeader)``

export const SubjectsFooter = styled(DefaultFooter)`
  display: flex;
  justify-content: center;
  align-items: center;
  background: white;
  border-top: black 2px solid;
  height: calc(70vh / 10.8);
`

export const HeaderText = styled(DefaultH3)`
  margin-left: calc(69vw / 19.2);
`

export const List = styled.div`
  flex: 1;
  overflow-y: auto;
`

export const SubjectRow = styled.div`
  cursor: pointer;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  height: calc(53vh / 10.8);
  font-weight: bold;
  color: ${props => props.active ? "#9d959d" : "#000000"};
  border-bottom: ${props => props.active ? "#F77A52" : "rgba(74,144,226,0.82)"} solid 2px;
`

export const SubjectText = styled(DefaultH4)`
  width: calc(305vw / 19.2);
  margin-left: calc(15vw / 19.2);
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

export const SubjectToolbox = styled.div`
  display: flex;
  flex-flow: row nowrap;
  height: 100%;
  flex: 1;
  align-items: center;
  justify-content: center;
`

export const ToolButton = styled.img`
  cursor: pointer;
  max-width: calc(35vw / 19.2);
  max-height: calc(35vw / 19.2);
  min-width: calc(35vw / 19.2);
  min-height: calc(35vw / 19.2);
  margin: 0 calc(5vw / 19.2);
  content: ${props => "url(" + props.defaultUrl + ");"};

  &:hover {
    content: ${props => "url(" + props.hoverUrl + ");"};
  }
`

export const Overlay = styled(Popup)`
  &-overlay {
    backdrop-filter: blur(5px);
  }
`

export const PopupContent = styled(DefaultForm)`
  flex-flow: column;
  align-items: center;
  width: calc(538vw / 19.2);
  height: calc(236vh / 10.8);
  box-shadow: 6px 6px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
  background: white;
`

export const PopupHeader = styled(DefaultHeader)`
  height: calc(53vh / 10.8);
`

export const PopupInput = styled(Text)`
  margin: calc(27vh / 10.8) calc(34vw / 19.2);
  height: calc(47vh / 10.8);
  width: calc(469vw / 19.2);
  font-size: calc(24vmin / 10.8);
  line-height: calc(24vmin / 10.8);
  border-radius: 2vmin;
  background: #ECECEC;
  border: none;
  box-sizing: border-box;
  padding-left: 1em;

  &:focus {
    outline: none;
  }
`

export const PopupButtons = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
`

export const SaveButton = styled(DefaultTextButton)`
  margin-top: calc(22vh / 10.8);
  margin-left: calc(34vw / 19.2);
`

export const CancelButton = styled(DefaultTextButton)`
  margin-top: calc(22vh / 10.8);
  margin-right: calc(34vw / 19.2);
  margin-left: calc(46vw / 19.2);
`

export const AddSubjectButton = styled(DefaultTextButton)`
  width: calc(212vw / 19.2);
  height: calc(37vh / 10.8);
`

export const TeachersForm = styled(DefaultForm)`
  flex-flow: column nowrap;
  width: calc(969vw / 19.2);
  height: calc(975vh / 10.8);
  margin-right: calc(69vw / 19.2);
`

export const TeacherRow = styled.div`
  display: flex;
  align-items: center;
  border-bottom: #4a90e2 2px solid;
  width: 100%;
  height: calc(53vh / 10.8);
`

export const TeacherName = styled(DefaultH4)`
  margin-left: calc(15vw / 19.2);
  margin-right: calc(35vw / 19.2);
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
`

export const TeachersToolButton = styled.div`
  border-radius: 100%;
  border: black solid 1px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: black;
  font-size: 1.5vh;
  font-family: Arial, serif;
  font-weight: bolder;

  width: calc(35vmin / 10.8);
  height: calc(35vmin / 10.8);
  margin-right: calc(29vw / 19.2);
  cursor: pointer;


  &:hover {
    color: white;
    background: black;
  }
`

export const NewTeacherContainer = styled.div`
  height: calc(35vh / 10.8);
  flex: 1;
  display: flex;
  flex-flow: column nowrap;
  margin-left: calc(15vw / 19.2);
  margin-right: calc(35vw / 19.2);
`

export const NewTeacherInput = styled(Text)`
  border: black solid 2px;
  border-radius: 0.5vmin;
  font-size: calc(24vmin / 10.8);
  line-height: calc(24vmin / 10.8);
  font-family: Arial, serif;
  font-weight: ${props => props.ready ? "bolder" : "lighter"};
  background: ${props => props.error ? "rgba(255,0,0,0.34)" : "#FFFFFF"};
`

export const SearchResults = styled.div`
`

export const SearchResult = styled.div`
  font-weight: ${props => props.selected ? "bolder" : "lighter"};
  background: white;
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
  overflow-x: hidden;
  text-overflow: ellipsis;
  height: calc(35vh / 10.8);
  border-right: black 1px solid;
  border-bottom: black 1px solid;
  border-left: black 1px solid;
`

export const SearchTeacherName = styled(DefaultH4)`
`

export const SearchTeacherUsername = styled(DefaultH4)`
  color: #9d959d;
`

export const Label = styled(SearchResult)`
  cursor: default;
  color: #9d959d;
`

export const TeachersFooter = styled(DefaultFooter)``
