import styled from 'styled-components'
import {DefaultForm, DefaultH3, DefaultH4, DefaultHeader} from "../Common.styles";
import * as Inputs from "../inputs";

export const MainFrame = styled.div`
  position: fixed;
  display: flex;
  flex-flow: row nowrap;
  left: calc(390vw / 19.2);
  top: calc(50vh / 10.8);
  width: calc(1140vw / 19.2);
  height: calc(975vh / 10.8);
`

export const ChatChooseForm = styled(DefaultForm)`
  position: relative;
  width: calc(360vw / 19.2);
  display: flex;
  flex-flow: column nowrap;
  height: 100%;
`

export const Header = styled(DefaultHeader)`
  min-height: calc(79vh / 10.8);
`

export const HeaderText = styled(DefaultH3)`
  margin-left: calc(20vw / 19.2);
`

export const ChatRow = styled.div`
  width: 100%;
  background: ${props => props.selected ? "#ECECEC" : "#FFF"};
  cursor: pointer;
  display: flex;
  align-items: center;
  height: calc(67vh / 10.8);
  border-bottom: #4a90e2 2px solid;
`

export const ChatImage = styled.img`
  width: calc(56vmin / 10.8);
  height: calc(56vmin / 10.8);
  border-radius: 100%;
  margin-left: calc(7vw / 19.2);
  border: black solid 2px;
`

export const ChatName = styled(DefaultH4)`
  margin-left: calc(16vw / 19.2);
  overflow-x: hidden;
  overflow-y: hidden;
  text-overflow: ellipsis;
`

export const NewMessagesCount = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 100%;
  margin-right: calc(16vw / 19.2);
  height: calc(42vmin / 10.8);
  width: calc(42vmin / 10.8);
  background: ${props => props.notificationsOn ? "#4A90E2" : "#A8BACF"};
`

export const NewMessagesCountText = styled(DefaultH4)`
  color: white;
  font-weight: bold;
  -webkit-text-stroke-width: 0.5px;
  -webkit-text-stroke-color: black;
`

export const ChatForm = styled(DefaultForm)`
  position: relative;
  margin-left: calc(30vw / 19.2);
  width: calc(750vw / 19.2);
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
`

export const MessagesList = styled.div`
  display: flex;
  flex: 1;
  flex-flow: column;
  overflow-y: auto;
  align-content: flex-end;
`

export const MessagesWritingBox = styled.div`
  display: flex;
  align-items: flex-end;
  border-top: #4a90e2 2px solid;
  width: 100%;
  min-height: calc(90vh / 10.8);
  max-height: calc(90vh / 10.8 * 5);
  border-radius: 0 0 2vmin 2vmin;
`

export const MessageInput = styled.div.attrs({contentEditable: true})`
  background: white;
  white-space: pre-line;
  cursor: text;
  overflow-x: hidden;
  text-overflow: clip;
  width: calc(605vw / 19.2);
  min-height: 4vh;
  max-height: 85%;
  overflow-y: auto;
  line-height: 2.1vh;
  font-size: 2vh;
  border-radius: 1vmin;
  margin-left: calc(72vw / 19.2);
  margin-top: calc(22vh / 10.8);
  margin-bottom: calc(22vh / 10.8);
  border: 1px solid #4A90E2;
  outline: none;
  padding: calc(5vh / 10.8) calc(10vw / 19.2);

  &:focus {
    outline: none;
  }

  /* width */

  ::-webkit-scrollbar {
    width: 20px;
    cursor: pointer
  }

  /* Track */

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 10px;
    cursor: pointer
  }

  /* Handle */

  ::-webkit-scrollbar-thumb {
    background: #4A90E2;
    border-radius: 10px;
    cursor: pointer
  }

  /* Handle on hover */

  ::-webkit-scrollbar-thumb:hover {
    background: #4A90E2;
    cursor: pointer;
  }
`

export const SendButton = styled.img`
  cursor: pointer;
  height: calc(47vmin / 10.8);
  width: calc(47vmin / 10.8);
  content: url("/img/send.png");
  margin-left: calc(15vw / 19.2);
  margin-right: calc(15vw / 19.2);
  margin-bottom: calc(50vh / 21.6);

  &:hover {
    content: url("/img/send_hover.png");
  }

`

export const Message = styled.div`
  margin-bottom: calc(10vh / 10.8);
  padding: calc(10vw / 19.2);
  left: calc(10vw / 19.2);
  width: 85%;
  margin-left: 1%;

  white-space: pre-line;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 18px;
  background: rgba(196, 196, 196, 0.5);
  color: #000000;
  border-radius: 1vmin;
`

export const SentMessage = styled(Message)`
  background: rgba(74, 144, 226, 0.5);
  margin-right: 1%;
  margin-left: auto;
`

export const NewChatContainer = styled.div`
  height: calc(35vh / 10.8);
  flex: 1;
  display: flex;
  flex-flow: column nowrap;
  margin-left: calc(15vw / 19.2);
  margin-right: calc(35vw / 19.2);
`

export const NewChatInput = styled(Inputs.Text)`
  border: black solid 2px;
  width: 100%;
  border-radius: 0.5vmin;
  font-size: calc(24vmin / 10.8);
  line-height: calc(24vmin / 10.8);
  font-family: Arial, serif;
  font-weight: ${props => props.ready ? "bolder" : "lighter"};
  background: ${props => props.error ? "rgba(255,0,0,0.34)" : "#FFFFFF"};
`

export const SearchResults = styled.div`
`

export const SearchResult = styled.div`
  background: white;
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
  overflow-x: hidden;
  text-overflow: ellipsis;
  height: calc(35vh / 10.8);
  border-right: black 1px solid;
  border-bottom: black 1px solid;
  border-left: black 1px solid;
  font-weight: ${props => props.selected ? "bolder" : "lighter"};
`

export const SearchName = styled(DefaultH4)`
  font-size: 1.5vh;
`

export const SearchUsername = styled(DefaultH4)`
  font-size: 1.5vh;
  color: #9d959d;
`

export const AddButton = styled.div`
  border-radius: 100%;
  border: black solid 1px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: black;
  font-size: 1.5vh;
  font-family: Arial, serif;
  font-weight: bolder;

  width: calc(35vmin / 10.8);
  height: calc(35vmin / 10.8);
  margin-right: calc(29vw / 19.2);
  cursor: pointer;


  &:hover {
    color: white;
    background: black;
  }
`

export const Label = styled(SearchResult)`
  cursor: default;
  color: #9d959d;
`

export const Loading = styled.div`
  backdrop-filter: blur(5px);
  display: flex;
  align-items: center;
  justify-content: center;
  padding-bottom: 50%;
  font-size: 2.5vh;
`