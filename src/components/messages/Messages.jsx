import React, {useEffect, useRef, useState} from "react";
import {connect} from "react-redux";
import {
    AddButton,
    ChatChooseForm,
    ChatForm,
    ChatImage,
    ChatName,
    ChatRow,
    Header,
    HeaderText,
    Label,
    Loading,
    MainFrame,
    Message,
    MessageInput,
    MessagesList,
    MessagesWritingBox,
    NewChatContainer,
    NewChatInput,
    NewMessagesCount,
    NewMessagesCountText,
    SearchName,
    SearchResult,
    SearchResults,
    SearchUsername,
    SendButton,
    SentMessage
} from "./Messages.styles";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import SockJsClient from "react-stomp";

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
    profile: state.profile
})
const Messages = ({dictionary, profile, ...props}) => {

    const [chatsLoading, setChatsLoading] = useState(true);

    const [messagesLoading, setMessagesLoading] = useState(true);

    const [selectedChatId, setSelectedChatId] = useState(null);

    const [page, setPage] = useState(0);

    const [wsReady, setWsReady] = useState(false);

    const [ready, setReady] = useState(false);

    const [error, setError] = useState(false);

    const [results, setResults] = useState([]);

    const [selectedResult, setSelectedResult] = useState(0);

    const [searchValue, setSearchValue] = useState("");

    const [userToChat, setUserToChat] = useState({});

    const [messages, setMessages] = useState([{
            text: "Loading",
            attachments: ["Loading"],
            chatId: ":Loading",
            sender: {
                id: 0,
                firstName: "Loading",
                lastName: "Loading"
            }
        }])
    ;

    const messagesList = useRef(null);

    const [chats, setChats] = useState([{
        name: "Loading",
        imageId: "Loading",
        newMessagesAmount: 0,
        notificationsEnabled: true
    }]);

    useEffect(() => {
        setError(false);
        if (searchValue !== "") {
            http.get(keywords.USERS + keywords.SEARCH + "?query=" + searchValue).then(
                response => {
                    setResults(response.data
                        .filter(u => !chats
                            .map(c => c.name).includes(u.firstName + " " + u.lastName))
                        .filter(u => profile.firstName !== u.firstName || profile.lastName !== u.lastName));
                    setSelectedResult(selectedResult => selectedResult > response.data.length - 1 ?
                        response.data.length - 1 : selectedResult < 0 ? 0 : selectedResult);
                }
            );
        } else {
            setResults([]);
        }
    }, [chats, searchValue]);

    function readyToAdd(index) {
        setSearchValue(results[index].firstName + " " + results[index].lastName);
        setUserToChat(results[index].id);
        setReady(true);
    }

    function handleInputKeydown(e) {
        if (!e.altKey) {
            switch (e.key) {
                case "Enter":
                    e.preventDefault();
                    if (!ready) {
                        if (results.length > 0) {
                            readyToAdd(selectedResult);
                        } else {
                            setError(true);
                        }
                    } else {
                        addChat();
                    }
                    break;
                case "ArrowDown":
                    e.preventDefault();
                    setSelectedResult(selectedResult => selectedResult === results.length - 1 ? 0 : selectedResult + 1);
                    break;
                case "ArrowUp":
                    e.preventDefault();
                    setSelectedResult(selectedResult => selectedResult === 0 ? results.length - 1 : selectedResult - 1);
                    break;
                default:
                    break;
            }
        }
    }

    function addChat() {
        if (ready) {
            let dto = {
                participantsIds: [profile.id, userToChat]
            }
            http.put(keywords.CHATS, dto).then(
                response => {
                    setChatsLoading(true);
                    setMessagesLoading(true);
                    setReady(false);
                    setSearchValue("");
                    setSelectedChatId(response.data);
                }
            ).catch(
                () => {
                }
            )
        } else {
            setError(true);

        }

    }

    useEffect(() => {
        if (chatsLoading) {
            http.get(keywords.CHATS).then(
                response => {
                    setChats(response.data);
                    setChatsLoading(false);
                    setSelectedChatId(selectedChatId => selectedChatId ? selectedChatId : (response.data[0]) ?
                        response.data[0].id : "");
                }
            );
        }
    }, [chatsLoading]);

    useEffect(() => {
        if (!chatsLoading && messagesLoading && selectedChatId !== "") {
            http.get(keywords.CHATS + "/" + selectedChatId + "?page=" + page).then(
                response => {
                    setMessages(response.data);
                    setMessagesLoading(false);
                    messagesList.current.scrollTop = messagesList.current.scrollHeight;
                    setChats(chats => {
                            chats.filter(c => c.id === selectedChatId).forEach(c => c.newMessagesAmount = 0);
                            return chats;
                        }
                    )
                }
            )
        }
    }, [chatsLoading, messagesLoading, page, selectedChatId])

    const onMessageReceived = msg => {
        setMessages(messages => [...messages, msg])
    }

    const messageIn = useRef(null);

    const sendMessage = () => {

        let text = messageIn.current.innerText;
        if (text !== "") {
            let data = {
                text: text,
                attachments: [],
                chatId: selectedChatId,
            }

            http.post(keywords.CHATS + "/" + selectedChatId, data).then(
                () => {
                    messageIn.current.textContent = "";
                    messagesList.current.scrollTop = messagesList.current.scrollHeight;
                }
            )
        }

    };

    const [searchFocus, setSearchFocus] = useState(false);


    function handleChatClick(chat, index) {
        setSelectedChatId(chat.id);
        setChats(chats => {
            chats[index].newMessagesAmount = 0;
            return chats
        })
        setMessagesLoading(true);
    }

    const handleEnterKeydown = (e) => {
        if (e.key === "Enter" && e.shiftKey) {
            sendMessage();
        }
    };

    return (
        <MainFrame>
            <ChatChooseForm>
                <Header>
                    <HeaderText>{dictionary.CHATS}</HeaderText>
                </Header>
                {chatsLoading ? <div/>
                    :
                    chats.map(
                        (chat, index) =>
                            <ChatRow key={chat.id} selected={chat.id === selectedChatId}
                                     onClick={() => handleChatClick(chat, index)}>
                                <ChatImage src={chat.imageId ? "images?id=" + chat.imageId : "img/profileImage.png"}/>
                                <ChatName>{chat.name}</ChatName>
                                <div style={{flex: 1}}/>
                                {chat.newMessagesAmount > 0 ?
                                    <NewMessagesCount notificationsOn={chat.notificationsEnabled}>
                                        <NewMessagesCountText>
                                            {chat.newMessagesAmount}
                                        </NewMessagesCountText>
                                    </NewMessagesCount> : <div/>}
                            </ChatRow>
                    )}
                <ChatRow>
                    <NewChatContainer style={{cursor: "default"}}>
                        <NewChatInput ready={ready} value={searchValue}
                                      error={error}
                                      onChange={e => {
                                          setSearchValue(e.target.value);
                                          setReady(false);
                                          setError(false);
                                          setSearchFocus(true);
                                      }}
                                      onKeyDown={(e) => {
                                          handleInputKeydown(e);
                                      }}
                                      onFocus={() => {
                                          setSearchFocus(true);
                                          setError(false);
                                      }}
                                      onBlur={() => setTimeout(() => setSearchFocus(false), 500)}/>
                        {searchFocus ?
                            <SearchResults>
                                {results.length !== 0 ? results.map(
                                    (result, index) =>
                                        <SearchResult selected={selectedResult === index} key={index}
                                                      onClick={() => readyToAdd(index)}>
                                            <SearchName>{`${result.firstName} ${result.lastName}`}</SearchName>
                                            <div style={{flex: 1}}/>
                                            <SearchUsername>{`(${result.username})`}</SearchUsername>
                                        </SearchResult>
                                    ) :
                                    !ready ? <Label>
                                        {searchValue === "" ? dictionary.START_TYPING : dictionary.NOFOUND}...
                                    </Label> : <div/>}
                            </SearchResults> :
                            <div/>}
                    </NewChatContainer>
                    <AddButton onClick={addChat}>+</AddButton>
                </ChatRow>
            </ChatChooseForm>
            <ChatForm>
                <SockJsClient
                    url={keywords.WEBSOCKET}
                    topics={[`${keywords.MESSAGES}/${selectedChatId}`]}
                    onMessage={onMessageReceived}
                    autoReconnect={false}
                    onConnect={() => setWsReady(true)}
                    onDisconnect={() => setWsReady(false)}
                    debug={true}/>
                <Header>
                    {selectedChatId ?
                        <HeaderText>{messagesLoading ? dictionary.LOADING : chats.filter(c => c.id === selectedChatId)[0].name}</HeaderText>
                        : <div/>}
                </Header>
                <MessagesList ref={messagesList}>
                    <div style={{flex: 1}}/>
                    {messagesLoading || !wsReady ? <Loading>{dictionary.LOADING}...</Loading> :
                        messages.map(
                            (message, index) =>
                                message.sender.id === profile.id ?
                                    <SentMessage key={index}>{message.text}</SentMessage>
                                    :
                                    <Message key={index}>{message.text}</Message>
                        )}
                </MessagesList>
                <MessagesWritingBox>
                    <MessageInput onKeyDown={handleEnterKeydown} ref={messageIn}/>
                    <SendButton onClick={sendMessage}/>
                </MessagesWritingBox>
            </ChatForm>
        </MainFrame>
    )
}

export default connect(mapStateToProps)(Messages);