import styled from "styled-components"
import Popup from "reactjs-popup";

export const Overlay = styled(Popup)`
  &-overlay {
    backdrop-filter: blur(5px);
    z-index: 5;
  }
`

export const NotificationsBody = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: calc(555vw / 19.2);
  height: calc(975vh / 10.8);
  position: relative;
  background: #FFFFFF;
  box-shadow: 6px 6px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const NotificationsHeader = styled(NotificationsBody)`
  display: flex;
  flex-flow: row nowrap;
  height: calc(75vh / 10.8);
  width: 100%;
  background: #2e2e2e;
  box-shadow: none;
  border-radius: 2vmin 2vmin 0 0;
  align-items: center;
`

export const HeaderText = styled.h3`
  position: relative;
  cursor: default;
  left: 10.6%;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: calc(32vmin / 10.8);
  line-height: calc(32vmin / 10.8);
  display: flex;
  align-items: center;
  color: #ffffff;
`;

export const CloseButton = styled.img`
  position: relative;
  cursor: pointer;
  right: calc(20vw / 19.2);
  width: calc(35vh / 10.8);
  height: calc(35vh / 10.8);
  content: url("/img/cancel_white.png");

  &:hover {
    content: url("/img/cancel_white_hover.png");
  }
`

export const NotificationsList = styled.div`
  display: flex;
  flex-flow: column;
  flex: 1;
  width: 100%;
  overflow-y: scroll;
`

export const NotificationRow = styled.div`
  width: 100%;
  height: calc(80vh / 10.8);
  border-bottom: 2px #4a90e2 solid;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  background: ${props => props.isNew ? "#ECECEC" : "#FFFFFF"};
`

export const NotificationInfo = styled.div`
  margin-left: calc(12vw / 19.2);
  margin-top: calc(12vh / 10.8);
  width: 100%;
  cursor: pointer;
`

export const NotificationText = styled.h3`
  width: calc(100% - 5%);
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: calc(18vmin / 10.8);
  line-height: calc(22vmin / 10.8);
  display: flex;
  align-items: center;
`

export const NotificationSummary = styled(NotificationText)`
  cursor: pointer;
  font-weight: bold;
  font-size: calc(18vmin / 10.8);
  line-height: calc(22vmin / 10.8);
`

export const DeleteNotificationButton = styled.img`
  position: relative;
  height: calc(35vmin / 10.8);
  width: calc(35vmin / 10.8);
  right: calc(17vw / 19.2);
  top: calc(4vh / 10.8);
  cursor: pointer;
  content: url("/img/delete_notifications.png");
  &:hover {
    content: url("/img/delete_notifications_hover.png");
  }
`

export const NotificationsFooter = styled(NotificationsHeader)`
  height: calc(15vh / 10.8);
  border-radius: 0 0 2vmin 2vmin;
`