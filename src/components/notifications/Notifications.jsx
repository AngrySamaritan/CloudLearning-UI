import React, {useEffect, useState} from "react";
import {
    CloseButton, DeleteNotificationButton,
    HeaderText, NotificationInfo,
    NotificationRow,
    NotificationsBody,
    NotificationsFooter,
    NotificationsHeader,
    NotificationsList,
    NotificationSummary,
    NotificationText,
    Overlay
} from "./Notifications.styles";
import {connect} from "react-redux";
import SockJsClient from "react-stomp";
import {keywords} from "../../config/KeyWords";
import {buildNotification} from "./NotificationBuilder";
import {NotificationManager} from "react-notifications";
import http from "../../util/Http";

const mapPropsToState = (state) => ({
    dictionary: state.dictionary,
    profile: state.profile,
});

function Notifications({dictionary, ready, newNotificationsHandler, ...props}) {


    const onMessageReceived = (msg) => {
        if (props.open) {
            setNotifications(notifications => ([
                msg,
                ...notifications.filter(notification => notification.id !== msg.id)
            ]))
            http.patch(keywords.NOTIFICATIONS, {}, {params: {id: msg.id}})
                .then()
                .catch(e => console.log("Cant mark new notification as readied: ", e));
        } else {
            newNotificationsHandler();
        }
        const notification = buildNotification(msg.type, msg.params, dictionary);
        if (notification.type !== "NEW_MESSAGES") {
            NotificationManager.info(notification.text, notification.summary, 5000, () => {
                notification.onClick();
            });
        } else if (window.location.href.indexOf("messenger") === -1) {
            NotificationManager.info(notification.text, notification.summary, 5000, () => {
                notification.onClick();
            });
        }
    }

    const [notifications, setNotifications] = useState([]);

    const [mustUpdate, setUpdate] = useState(true);

    useEffect(() => {
        if (props.open && mustUpdate) {
            http.get("/notifications").then(
                response => setNotifications(response.data)
            );
            setUpdate(false)
        }
    }, [props.open, mustUpdate]);

    return (
        <div>
            {ready ? <SockJsClient
                url={keywords.WEBSOCKET}
                topics={[`${keywords.USER}/${props.profile.id}${keywords.NOTIFICATIONS}`]}
                onMessage={onMessageReceived}
                autoReconnect={false}
                debug={true}/> : <div/>}
            <Overlay {...props} onClose={() => {
                props.onClose();
                setUpdate(true);
            }}>
                {close => <NotificationsBody>
                    <NotificationsHeader>
                        <HeaderText>{dictionary.NOTIFICATIONS}</HeaderText>
                        <div style={{flex: 1}}/>
                        <CloseButton onClick={close}/>
                    </NotificationsHeader>
                    <NotificationsList>
                        {notifications.map((notificationInfo, index) => {
                            const notification = buildNotification(notificationInfo.type, notificationInfo.params, dictionary);
                            return (
                                <NotificationRow key={index} isNew={notificationInfo.status === "READY_TO_DELIVERY"}>
                                    <NotificationInfo onClick={() => notification.onClick()}>
                                        <NotificationSummary>
                                            {notification.summary}
                                        </NotificationSummary>
                                        <NotificationText>
                                            {notification.text}
                                        </NotificationText>
                                    </NotificationInfo>
                                    <DeleteNotificationButton onClick={() =>

                                        http.delete(keywords.NOTIFICATIONS, {params: {id: notificationInfo.id}})
                                            .then(() => setUpdate(true))
                                            .catch(e => console.log("Error due deleting notification", e))
                                    }/>
                                </NotificationRow>);
                        })}
                    </NotificationsList>
                    <NotificationsFooter/>
                </NotificationsBody>}
            </Overlay>
        </div>
    )
}

export default connect(mapPropsToState)(Notifications);