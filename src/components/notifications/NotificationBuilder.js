export function buildNotification(type, params, dictionary) {
    let template = dictionary.notifications[type].text;
    const summary = dictionary.notifications[type].summary;
    const f = onClickFunctions[type];
    if (!template) {
        throw new Error("Unknown notification type:" + type);
    }
    Object.entries(params).forEach(([key, value]) => {
        template = template.replace("{" + key + "}", value);
    });
    return {summary: summary, text: template, onClick: f ? f.bind(null, params) : undefined, type: type};
}

const onClickFunctions = {
    "ACCESS_TO_TEST_GRANTED": ({testId}) => {
        window.location.replace("/test/" + testId + "/info");
    },
    "SUBJECTS_ASSIGNEE_CHANGED": ({userId}) => {
        window.location.replace("/profile/" + userId);
    },
    "NEW_MESSAGES": () => {
        window.location.replace("/messenger");
    },

}