import styled from "styled-components";
import Popup from "reactjs-popup";
import {Checkbox} from "../inputs";

export const PopUp = styled(Popup)`
  &-overlay {
    backdrop-filter: blur(5px);
    z-index: 5;
  }
`

export const Content = styled.div`
  display: flex;
  flex-flow: column nowrap;
  height: calc(975vh / 10.8);
  width: calc(555vw / 19.2);
  background: #FFFFFF;
  box-shadow: 6px 6px 4px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const Header = styled.div`
  width: 100%;
  border-radius: 2vmin 2vmin 0 0;
  height: calc(84vh / 10.8);
  background: #2E2E2E;
  display: flex;
  align-items: center;
`

export const HeaderText = styled.h3`
  cursor: default;
  color: #FFFFFF;
  margin-left: calc(32vw / 19.2);
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: calc(32vmin / 10.8);
  line-height: calc(32vmin / 10.8);
`

export const SubjectsBody = styled.div`
  display: flex;
  flex-flow: column nowrap;
  flex: 1;
  overflow-y: scroll;
`

export const SubjectRow = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row;
  align-items: center;
  justify-content: center;
  height: calc(57vh / 10.8);
  border-bottom: black solid 1px;
  cursor: pointer;
`

export const SubjectText = styled.h3`
  margin-left: calc(20vw / 19.2);
  height: 100%;
  display: flex;
  align-items: center;

  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: calc(32vmin / 10.8);
  line-height: calc(32vmin / 10.8);

  letter-spacing: 0.04em;

  color: #4A59E2;
`

export const SubjectCheckbox = styled(Checkbox)`
  transform: scale(1.6);
  margin-right: calc(25vmin / 10.8);
`

export const CloseButton = styled.img`
  position: relative;
  cursor: pointer;
  right: calc(20vw / 19.2);
  width: calc(35vh / 10.8);
  height: calc(35vh / 10.8);
  content: url("/img/cancel_white.png");

  &:hover {
    content: url("/img/cancel_white_hover.png");
  }
`

export const Footer = styled.div`
  display: flex;
  align-items: center;
  height: calc(91vh / 10.8);
  width: 100%;
  border-top: black 2px solid;
`

export const Button = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 2vmin;
  line-height: 2vh;
  text-align: center;
  
  width: calc(212vw / 19.2);
  height: calc(37vh / 10.8);
  color: black;
  border-radius: 2vmin;
  border: black 1px solid;
  &:hover {
    background: black;
    color: white;
  }
`

export const CancelButton = styled(Button)`
  margin-left: calc(31vw / 19.2);
`

export const SubmitButton = styled(Button)`
  margin-left: calc(69vw / 19.2);
`