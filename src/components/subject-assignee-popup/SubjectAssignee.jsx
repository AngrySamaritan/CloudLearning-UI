import React, {useEffect, useState} from "react";
import {
    CancelButton,
    Content,
    Footer,
    Header,
    HeaderText,
    SubjectCheckbox,
    SubjectRow,
    SubjectsBody,
    SubjectText,
    SubmitButton
} from "./SybjectAssignee.styles";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import {NotificationManager} from "react-notifications";

const SubjectAssignee = ({dictionary, userId, open, close, updating}) => {

    const [subjects, setSubjects] = useState([]);
    const [update, setUpdate] = useState(false);

    useEffect(() => {
        if (open) {
            http.get(keywords.SUBJECTS).then(
                response => {
                    setSubjects(response.data.map(
                        (subject) => ({
                            name: subject.name,
                            id: subject.id,
                            marked: subject.teachers.map(teacher => teacher.id.toString())
                                .includes(userId.toString())
                        })
                    ));
                }
            )
        }
    }, [userId, open]);

    useEffect(() => {
        setUpdate(false);
    }, [update]);

    function subjectCheckHandler(index) {
        setUpdate(true);
        setSubjects(subjects => {
            subjects[index].marked = !subjects[index].marked;
            return subjects;
        });
    }

    const submit = () => {
        const data = subjects.filter(subject => subject.marked).map(subject => subject.id);
        http.patch(keywords.USERS + "/" + userId + keywords.SUBJECTS, data).catch(
            (error) => {
                console.error(error);
                NotificationManager.error(error, dictionary.ERROR, 5000);
            }
        ).then(() => {
            updating();
            close();
            NotificationManager.success(dictionary.SUBJECTS_ASSIGNEE_UPDATED, dictionary.SUCCESS, 5000);
        });
    }

    return (
        <Content>
            <Header>
                <HeaderText>
                    {dictionary.SUBJECTS}
                </HeaderText>
            </Header>
            <SubjectsBody>
                {subjects.map((subject, index) =>
                    <SubjectRow key={index} onClick={(e) => {
                        e.stopPropagation();
                        subjectCheckHandler(index);
                    }}>
                        <SubjectText>{subject.name}</SubjectText>
                        <div style={{flex: 1}}/>
                        <SubjectCheckbox checked={!!subject.marked}/>
                    </SubjectRow>
                )}
            </SubjectsBody>
            <Footer>
                <CancelButton onClick={close}>{dictionary.CANCEL}</CancelButton>
                <SubmitButton onClick={submit}>{dictionary.OK}</SubmitButton>
            </Footer>
        </Content>
    )
}

export default SubjectAssignee;