import styled from 'styled-components';

export const SearchCard = styled.div`
  box-shadow: 6px 6px 4px 10px rgba(0, 0, 0, 0.25);
  top: 4vh;
  left: 17vw;
  width: 35vw;
  height: 92.9vh;
  border-radius: 20px;
`;

export const CardHeadr = styled.div`
  background: #2e2e2e;
  height: 7.7%;
  border-radius: 20px 20px 0 0;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-around;
`;

export const CardH2 = styled.h3`
  position: relative;
  left: 3%;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 32px;
  line-height: 32px;
  display: flex;
  align-items: center;
  color: #ffffff;
`;

export const CardBody = styled.div`
  background: white;
  left: auto;
  width: 100%;
  height: 90.7%;
  overflow-y: auto;
  overflow-x: hidden;
  border-left: 2px black transparent;
  border-right: 2px black transparent;
`;
export const CardFooter = styled.div`
  background: #2e2e2e;
  height: 1.6%;
  border-radius: 0 0 20px 20px;
`;

export const CardInput = styled.div`
  margin: 0;
  left: 100%;
  width: 65%;
  height: 50%;
  display: flex;
  align-items: center;
  overflow: hidden;
  padding-bottom: 0px;
`;

export const SearchInput = styled.input`
  margin-left: 0;
  border-radius: 5px 0px 0px 5px;
  width: 65%;
  height: 100%;
  border: none;
`;

export const SelectType = styled.select`
  height: 100%;
  width: 30%;
  border-radius: 0px 5px 5px 0px;
  border: none;
`;

export const SearchItem = styled.div`
  position: relative;
  left: 3%;
  height: 4.3%;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 32px;
  line-height: 32px;
  display: flex;
  align-items: center;
  color: #2e2e2e;
  margin-top: 3%;

  &:active {
    background: #4a90e2;
  }
`;

export const SearchItemNone = styled.div`
  position: relative;
  left: 3%;
  height: 4.3%;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 32px;
  line-height: 32px;
  display: flex;
  align-items: center;
  color: #2e2e2e;
  margin-top: 3%;
`;

export const TypeOfItem = styled.h2`
  position: absolute;
  right: 0;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 32px;
  line-height: 32px;
  display: flex;
  align-items: center;
  color: gray;
`;

export const ItemLabel = styled.label`
  display: flex;
  align-items: center;
  position: relative;
  width: 95%;
`;

export const CloseButton = styled.img`
  position: relative;
  cursor: pointer;
  right: calc(20vw / 19.2);
  width: calc(35vh / 10.8);
  height: calc(35vh / 10.8);
  content: url('/img/cancel_white.png');

  &:hover {
    content: url('/img/cancel_white_hover.png');
  }
`;
