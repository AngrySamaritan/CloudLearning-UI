import React, {useState, useEffect} from 'react'
import {keywords} from '../../config/KeyWords';
import http from '../../util/Http';
import {
    CardBody,
    CardFooter,
    CardHeadr,
    SearchCard,
    CardH2,
    CardInput,
    SearchInput,
    SelectType,
    SearchItem,
    TypeOfItem,
    ItemLabel,
    SearchItemNone,
    CloseButton
} from './Search.styles'
import {Link} from 'react-router-dom';


export default function Search({dictionary, close}) {
    const [searchOption, setSearchOption] = useState('user');
    const [searchInput, setSearchInput] = useState('');
    const [searchMembers, setSearchMembers] = useState([1, 1]);
    const [searchGroups, setSearchGroups] = useState([1, 1]);

    useEffect(() => {
        http.get(keywords.USERS + keywords.SEARCH + '?query=' + searchInput).then((res) => {
            let Members = res.data;
            setSearchMembers(Members);
        })

        http.get(keywords.GROUPS + keywords.SEARCH + '?query=' + searchInput).then((res) => {
            let Groups = res.data;
            console.log(Groups)
            setSearchGroups(Groups);
        })
    }, [searchInput])

    const ShowSearchMembersResult = () => {
        return (searchMembers.length === 0 && searchOption === 'user' ?
                <SearchItemNone>
                    <ItemLabel>{dictionary.NOFOUND}</ItemLabel>
                </SearchItemNone> : searchMembers.map(
                    (item, index) => {
                        return (
                            <Link to={`/profile/${item.id}`} onClick={close} key={index}>
                                <SearchItem>
                                    <input type="radio" style={{display: 'none'}} value={item.id} id={item.id}/>
                                    <ItemLabel
                                        htmlFor={item.id}>{item.firstName + " " + item.lastName + ` (${item.username})`}
                                        <TypeOfItem>User</TypeOfItem> </ItemLabel>
                                </SearchItem>
                            </Link>
                        )
                    }
                )
        )
    }

    const ShowSearchGroupsResult = () => {
        return (searchGroups.length === 0 && searchOption === 'group' ?
                <SearchItemNone>
                    <ItemLabel>{dictionary.NOFOUND}</ItemLabel>
                </SearchItemNone> : searchGroups.map(
                    (item, index) => {
                        return (
                            <Link to={`/group/${item.id}`} onClick={close} key={index}>
                                <SearchItem>
                                    <input type="radio" style={{display: 'none'}} value={item.id} id={item.id}/>
                                    <ItemLabel htmlFor={item.id}>{item.code} <TypeOfItem>Group</TypeOfItem> </ItemLabel>
                                </SearchItem>
                            </Link>
                        )
                    }
                )
        )
    }

    const ShowAllResult = () => {
        return (searchGroups.length + searchMembers.length === 0 ?
                <SearchItemNone>
                    <ItemLabel>{dictionary.NOFOUND}</ItemLabel>
                </SearchItemNone> : <><ShowSearchGroupsResult/><ShowSearchMembersResult/></>
        )
    }

    const handleChange = (e) => {
        if (e.target.name === 'searchQuery') {
            setSearchInput(e.target.value)
        }
        if (e.target.name === 'searchOption') {
            console.log(searchOption)
            setSearchOption(e.target.value)
        }
    }

    return (
        <SearchCard>
            <CardHeadr>
                <CardH2>
                    {dictionary.SEARCH}
                </CardH2>
                <CardInput>
                    <SearchInput name='searchQuery' onChange={handleChange}/>
                    <SelectType name='searchOption' onChange={handleChange}>
                        <option value="user">{dictionary.USERS}</option>
                        <option value="group">{dictionary.GROUPS}</option>
                        <option value="all">{dictionary.ALL}</option>
                    </SelectType>
                </CardInput>
                <CloseButton onClick={close}/>
            </CardHeadr>
            <CardBody>
                {
                    searchOption === 'user' ?
                        <ShowSearchMembersResult/>
                        : searchOption === 'group' ? <ShowSearchGroupsResult/> :
                        <ShowAllResult/>
                }
            </CardBody>
            <CardFooter>

            </CardFooter>
        </SearchCard>
    );
}
