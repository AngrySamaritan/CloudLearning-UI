import styled from "styled-components";
import Popup from "reactjs-popup";

export const MainForm = styled.div`
  display: flex;
  flex-flow: row nowrap;
  position: fixed;
  top: 5.2vh;
  left: 20.22vw;
  width: calc(100vw - 20.22vw * 2);
  height: 90%;
  opacity: 1;
`

export const ContentForm = styled.div`
  display: flex;
  flex-flow: column nowrap;
  flex: 555;
  height: 100%;
  background-color: #fff;
  opacity: 1;
  -webkit-border-radius: 2vh;
`

export const HeaderText = styled.h3`
  position: relative;
  left: 10.6%;
  top: 28%;
  cursor: default;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 32px;
  line-height: 32px;
  display: flex;
  align-items: center;
  color: #ffffff;
`;

export const GroupSelectorButton = styled.div`
  position: relative;
  cursor: pointer;
  margin-left: 3%;
  display: block;
  width: 2.5vmin;
  height: 2.5vmin;
  content: url("/img/selector.png");

  &:hover {
    content: url("/img/selector_hower.png");
  }
`
export const Header = styled.div`
  width: 100%;
  height: 7.7%;
  background: #2e2e2e;
  border-radius: 2vh 2vh 0 0;
`

export const Content = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: flex-start;
  align-items: center;
  flex: 1;
  overflow-y: auto;
  max-height: calc(100% - 7.7% - 2vh);
`

export const Footer = styled(Header)`
  height: 2vh;
  border-radius: 0 0 2vh 2vh;
`

export const Row = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  border-bottom: 2px solid #4a90e2;
  position: relative;
  width: 90%;
  height: 4.3%;
  font-family: Arial, serif;
  font-size: 32px;
  color: #2e2e2e;
  margin-top: 3%;
`

export const RowText = styled.h3`
  cursor: default;
`

export const SelectGroupMenuText = styled(RowText)`
  cursor: pointer;
`

export const SelectGroupMenuRow = styled(Row)`
  cursor: pointer;

  &:hover {
    color: #9d959d;
  }
`

export const ToAllowedButton = styled.img`
  content: url("/img/toAllowed.png");
  height: 100%;
  cursor: pointer;

  &:hover {
    content: url("/img/toAllowed_hover.png");
  }
`

export const DisallowButton = styled(ToAllowedButton)`
  cursor: pointer;
  transform: scale(-1, 1);
`
export const GroupSelectForm = styled(ContentForm)`
  height: 90vh;
  width: 50vh;
  z-index: 10;
`

export const GroupSelectPopUp = styled(Popup)`
  &-overlay {
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 5;
  }
`
export const CancelButton = styled.img`
  position: fixed;
  top: 6%;
  left: 58%;
  width: 5vmin;
  height: 5vmin;
  content: url("/img/cancel_white.png");

  &:hover {
    content: url("/img/cancel_white_hover.png");
  }
`

export const SubmitButton = styled.img`
  position: fixed;
  top: 6%;
  left: 76%;
  width: 5vmin;
  height: 5vmin;
  content: url("/img/ok_white.png");

  &:hover {
    content: url("/img/ok_white_hover.png");
  }
`

