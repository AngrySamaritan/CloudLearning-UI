import React, {useEffect, useState} from "react";
import {
    CancelButton,
    Content,
    ContentForm,
    DisallowButton,
    Footer,
    GroupSelectForm,
    GroupSelectorButton,
    GroupSelectPopUp,
    Header,
    HeaderText,
    MainForm,
    Row,
    RowText,
    SelectGroupMenuRow,
    SelectGroupMenuText,
    SubmitButton,
    ToAllowedButton
} from "./AcessManagement.styles";
import {connect} from "react-redux";
import http from "../../util/Http"
import {keywords} from "../../config/KeyWords";
import {isEqual} from "lodash";
import {NotificationManager} from "react-notifications";

const mapStateToProps = state => ({
    dictionary: state.dictionary,
})

function AccessManagement(props) {

    const [state, setState] = useState({
        groups: [{id: null, code: "N/A"}],
        selectedGroup: {id: null, code: "N/A"},
        allowedUsers: [{id: null, firstName: "N/A", lastName: "N/A", group: {id: null, code: "N/A"}}],
        groupUsers: [{id: null, firstName: "N/A", lastName: "N/A", group: {id: null, code: "N/A"}}],
    });

    const [loaded, setLoaded] = useState({groups: false, groupUsers: false, allowedUsers: false});

    useEffect(() => {
        if (!loaded.groups) {
            http.get(keywords.GROUPS).then((response) => {
                setState(state => ({...state, groups: response.data, selectedGroup: response.data[0]}))
                setLoaded(loaded => ({...loaded, groups: true}))
            }).catch(reason => {
                console.log(reason)
            })
        }
    }, [loaded.groups]);

    const id = props.match.params.id;

    useEffect(() => {
        if (!loaded.allowedUsers) {
            http.get(keywords.TESTS + "/" + id + keywords.MANAGEMENT + keywords.USERS)
                .then((response) => {
                    setState(state => ({...state, allowedUsers: response.data}))
                    setLoaded(loaded => ({...loaded, allowedUsers: true}))
                }).catch(reason => {
                console.log(reason)
            })
        }
    }, [id, loaded.allowedUsers]);

    useEffect(() => {
        if (loaded.groupUsers && loaded.allowedUsers) {
            const newGroupUsers = state.groupUsers.filter((user) =>
                !state.allowedUsers.some(allowedUser => isEqual(user, allowedUser))
            );
            if (!isEqual(state.groupUsers, newGroupUsers)) {
                setState(state => ({...state, groupUsers: newGroupUsers}))
            }
        }
    }, [loaded.groupUsers, loaded.allowedUsers, state.allowedUsers, state.groupUsers])

    useEffect(() => {
        if (state.selectedGroup.id) {
            setLoaded(loaded => ({...loaded, groupUsers: false}));
            http.get(keywords.GROUPS + "/" + state.selectedGroup.id).then((response) => {
                setState(state => ({...state, groupUsers: response.data.users}));
                setLoaded(loaded => ({...loaded, groupUsers: true}));
            }).catch(reason => {
                console.log(reason)
            })
        }
    }, [state.selectedGroup]);

    const setSelectedGroup = (group) => {
        setState(state => ({...state, selectedGroup: group}));
    }

    const GroupUsersArrayView = ({users}) => {
        return users.map(
            (user, index) => (
                <Row key={"group" + index}>
                    <RowText>{user.firstName + " " + user.lastName}</RowText>
                    <div style={{flex: 1}}/>
                    <ToAllowedButton onClick={() => {
                        setState(state => ({...state, allowedUsers: [...state.allowedUsers, user]}))
                    }}/>
                </Row>
            )
        )
    }

    const AllowedUsersArrayView = ({users}) => {
        return users.map(
            (user, index) => (
                <Row key={"user" + index}>
                    <DisallowButton onClick={() => {
                        let allowedUsers = state.allowedUsers.filter(allowedUser => !isEqual(user, allowedUser));
                        if (user.group.id === state.selectedGroup.id) {
                            setState(state => ({
                                ...state,
                                allowedUsers: allowedUsers,
                                groupUsers: [...state.groupUsers, user]
                            }))
                        } else {
                            setState(state => ({
                                ...state,
                                allowedUsers: allowedUsers,
                                groupUsers: [...state.groupUsers, user]
                            }))
                        }
                    }}/>
                    <div style={{flex: 1}}/>
                    <RowText>{user.firstName + " " + user.lastName}</RowText>
                </Row>
            )
        )
    }

    const GroupList = ({groups, setSelectedGroup, close}) => {
        return groups.map(
            (group, index) => (
                <SelectGroupMenuRow key={"group" + index} onClick={() => {
                    setSelectedGroup(group);
                    close();
                }}>
                    <SelectGroupMenuText>{group.code}</SelectGroupMenuText>
                </SelectGroupMenuRow>
            )
        )
    }

    const submit = () => {
        http.patch(keywords.TESTS + "/" + id + keywords.MANAGEMENT + keywords.USERS, state.allowedUsers
            .map(user => user.id)).then(() => {
            NotificationManager.success(props.dictionary.ALLOWED_USERS_UPDATED, props.dictionary.INFO, 3000);
            setLoaded({groups: false, groupUsers: false, allowedUsers: false});
            props.history.goBack();
        });
    }

    return (
        <MainForm>
            <ContentForm>
                <Header>
                    <HeaderText>{loaded.groups ? state.selectedGroup.code : "N/A"}
                        <GroupSelectPopUp trigger={<GroupSelectorButton/>} modal>
                            {close =>
                                <GroupSelectForm>
                                    <Header>
                                        <HeaderText>{props.dictionary.SELECT_GROUP}</HeaderText>
                                        <CancelButton onClick={close}/>
                                    </Header>
                                    <Content>
                                        <GroupList groups={state.groups} setSelectedGroup={setSelectedGroup}
                                                   close={close}/>
                                    </Content>
                                </GroupSelectForm>
                            }
                        </GroupSelectPopUp>
                    </HeaderText>
                </Header>
                <Content>
                    {loaded.groupUsers ? <GroupUsersArrayView users={state.groupUsers}/> : <div/>}
                </Content>
                <Footer>

                </Footer>
            </ContentForm>
            <div style={{flex: 30}}/>
            <ContentForm>
                <Header>
                    <HeaderText>{props.dictionary.ALLOWED_USERS}</HeaderText>
                    <SubmitButton onClick={submit}/>
                </Header>
                <Content>
                    {loaded.groupUsers ? <AllowedUsersArrayView users={state.allowedUsers}/> : <div/>}
                </Content>
                <Footer>

                </Footer>
            </ContentForm>
        </MainForm>
    )
}

export default connect(mapStateToProps)(AccessManagement)