import React from "react";

export const Checkbox = props => (
    <input type="checkbox" {...props} />
)

export const Radiobutton = props => (
    <input type="radio" {...props} />
)

export const Text = props => (
    <input type="text" {...props} />
)

export const Password = props => (
    <input type="password" {...props} />
)

export const Number = props => (
    <input type="number" {...props} />
)