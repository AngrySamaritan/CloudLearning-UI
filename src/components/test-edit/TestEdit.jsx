import React, {useEffect, useRef, useState} from "react";
import {
    AddAnswerButton,
    AddImageButton,
    AnswerRow,
    AnswersList,
    AnswerText,
    AnswerTextContainer,
    ButtonInput, CancelButton,
    ClearButton,
    ContentForm,
    DeleteAnswerButton,
    DeleteQuestionButton,
    DurationField,
    EditTestInfoButton,
    EditToolbox,
    Image,
    ImageContainer,
    ImagePlaceholder,
    InfoText, Loader, LoadingOverlay,
    MetaInfo,
    MiddleContainer,
    NumberInput,
    OverlayImg,
    PointsAndTypeFlexBox,
    PointsContainer,
    PreviewOverlay,
    QuestionButtonLeft,
    QuestionButtonRight,
    QuestionInfo,
    QuestionListContainer,
    QuestionListContent,
    QuestionListFooter,
    QuestionListHeader,
    QuestionRow,
    QuestionsButton,
    QuestionsEditingForm,
    QuestionTextContainer,
    QuestionTextEditBox,
    QuestionTextFlexBox,
    QuestionTextPreview,
    QuestionTypeLabel,
    QuestionTypeRow,
    QuestionTypeSelector, SaveButton,
    SaveButtonQuestionList,
    TestDescription,
    TestDescriptionContainer,
    TestInfoForm,
    TestName,
    TestNameContainer,
    TestNameLabel,
    TestNameView
} from "./TestEdit.styles";
import {QuestionTypes} from "../../config/QuestionsTypes";
import http from "../../util/Http"
import {keywords} from "../../config/KeyWords";
import {NotificationManager} from 'react-notifications';
import {connect} from "react-redux";
import {useHistory} from "react-router-dom";
import {buildNotification} from "../notifications/NotificationBuilder";


const MINIMUM_TEST_DURATION = 1;

const initialAnswer = {
    id: null,
    text: "",
    isTrue: null
}

const initialQuestion = {
    id: null,
    text: "",
    points: 1,
    type: QuestionTypes.SINGLE_SELECT_QUESTION,
    answers: [{...initialAnswer}, {...initialAnswer}]
}

const initialTestInfo = {
    id: null,
    duration: 10,
    name: "",
    subjectId: null,
    description: "",
    questions: [{...initialQuestion}]
}


const mapStateToProps = state => ({
    dictionary: state.dictionary,
});

function countPoints(questions) {
    let points = 0;
    questions.forEach(question => {
        points += parseInt(question.points)
    });
    return points;
}

function TestEdit({dictionary, ...props}) {

    const [test, updateTest] = useState({
        ...initialTestInfo,
        questions: [{
            ...initialQuestion,
            answers: [Object.assign({}, initialAnswer), Object.assign({}, initialAnswer),
                Object.assign({}, initialAnswer), Object.assign({}, initialAnswer)]
        }]
    });

    const [questions, setQuestions] = useState(false);


    const validateInfo = () => {

        setQuestions(false);
        let ntf = undefined;

        if (!test.name) {
            ntf = buildNotification("EMPTY_TEST_NAME", {}, dictionary);
        } else if (!test.description) {
            ntf = buildNotification("EMPTY_TEST_DESCRIPTION", {}, dictionary);
        }
        if (ntf) {
            setSelected(0);
            NotificationManager.error(ntf.text, ntf.summary, 4000);
            return false;
        }
        return true;
    }

    const validateQuestions = () => {

        let problematicQuestionIndex = undefined;
        let template = undefined;

        const ValidationException = {};

        try {
            test.questions.forEach(
                (question, index) => {
                    if (!question.text) {
                        template = "EMPTY_QUESTION_TEXT";
                        problematicQuestionIndex = index;
                        throw ValidationException;
                    }
                    if (question.type === QuestionTypes.SINGLE_SELECT_QUESTION ||
                        question.type === QuestionTypes.MULTIPLE_SELECT_QUESTION) {
                        if (question.answers.filter(ans => ans.isTrue).length < 1) {
                            template = "NO_RIGHT_ANSWERS";
                            problematicQuestionIndex = index;
                            throw ValidationException;
                        }
                    }
                    if (question.answers.some(answer => !answer.text)) {
                        template = "EMPTY_ANSWER";
                        console.log(template);
                        problematicQuestionIndex = index;
                        throw ValidationException;
                    }
                }
            )
        } catch (e) {
            if (e !== ValidationException) {
                throw e;
            } else {
                const ntf = buildNotification(template, {index: problematicQuestionIndex + 1}, dictionary);
                NotificationManager.error(ntf.text, ntf.summary, 4000);
                setSelected(problematicQuestionIndex);
                setQuestions(true);
                return false;
            }
        }
        return true;
    }

    const saveTest = () => {
        setSaving(true);
        if (validateInfo() && validateQuestions()) {
            if (test.subjectId != null) {
                http.post(keywords.TESTS, test).then(() => {
                        NotificationManager.info("Test successfully saved", "Success", 4000);
                        updateTest(Object.assign({}, initialTestInfo));
                        props.history.push("/subject/id" + test.subjectId);
                    }
                ).catch((() => {
                    NotificationManager.error("Error! Please check if test is correct", "Error", 4000);
                    setSaving(false);
                }))
            } else {
                setSaving(false);
                NotificationManager.error("Error! Page are still not loaded", "Error", 4000);
            }
        } else {
            setSaving(false);
        }
    }

    useEffect(() => {
        updateTest(test => ({...test, subjectId: props.match.params.subjectId}))
    }, [props.match.params.subjectId])

    useEffect(() => {
        let id = props.match.params.id;
        if (id) {
            http.get(keywords.TESTS + "/" + id + keywords.FULL).then(
                response => updateTest(response.data)
            )
        }
    }, [props.match.params.id])

    const [selected, setSelected] = useState(0);

    const [saving, setSaving] = useState(false);

    return (
        <ContentForm>
            <LoadingOverlay open={saving} closeOnDocumentClick={false} closeOnEscape={false}>
                <Loader/>
            </LoadingOverlay>
            {questions ?
                <QuestionsEditing test={test} updateTest={updateTest} dictionary={dictionary}
                                  setQuestions={setQuestions} save={saveTest} selected={selected}
                                  setSelected={setSelected}/>
                :
                <TestInfo test={test} updateTest={updateTest} dictionary={dictionary} setQuestions={setQuestions}
                          save={saveTest} setSelected={setSelected}/>}
        </ContentForm>
    );

}

function QuestionsEditing({test, updateTest, dictionary, setQuestions, save, selected, setSelected}) {

    useEffect(() => {

    }, [selected])

    const setSelectedQuestionType = (type) => {
        let questions = test.questions;
        questions[selected].type = type;
        while (type !== QuestionTypes.WRITING_QUESTION && questions[selected].answers.length < 2) {
            addAnswer();
        }
        if (type === QuestionTypes.SINGLE_SELECT_QUESTION) {
            let isAnyTrue = false;
            questions[selected].answers.map(answer => {
                if (answer.isTrue) {
                    if (isAnyTrue) {
                        answer.isTrue = false;
                    } else {
                        isAnyTrue = true;
                    }
                }
                return answer;
            });
        }
        updateTest(test => ({...test, questions: questions}))
    }

    const setSelectedQuestionText = (text) => {
        let questions = test.questions;
        questions[selected].text = text;
        updateTest(test => ({...test, questions: questions}))
    }

    const setSelectedQuestionPoints = (value) => {
        let questions = test.questions;
        if (value > 0 || value === "") {
            questions[selected].points = value;
        } else {
            questions[selected].points = 1;
        }
        updateTest(state => ({
            ...state,
            questions: questions
        }))
    }

    const addAnswer = () => {
        let questions = test.questions;
        questions[selected].answers.push(Object.assign({}, initialAnswer));
        updateTest(test => ({...test, questions: questions}));
    }

    const deleteAnswer = (index) => {
        let questions = test.questions;
        questions[selected].answers.splice(index, 1);
        updateTest(test => ({...test, questions: questions}));
    }

    const setTrue = (index, value) => {
        let questions = test.questions;
        if (questions[selected].type === QuestionTypes.SINGLE_SELECT_QUESTION) {
            questions[selected].answers.map(answer => answer.isTrue = false);
            questions[selected].answers[index].isTrue = true;
        } else {
            questions[selected].answers[index].isTrue = value;
        }
        updateTest(test => ({...test, questions: questions}));
    }

    const setAnswerText = (index, text) => {
        let questions = test.questions;
        questions[selected].answers[index].text = text;
        updateTest(test => ({...test, questions: questions}));
    }

    const addNewQuestion = () => {
        let questions = test.questions;
        let question = Object.assign({}, initialQuestion);
        question.answers = [Object.assign({}, initialAnswer), Object.assign({}, initialAnswer),
            Object.assign({}, initialAnswer), Object.assign({}, initialAnswer)]
        questions.push(question);
        updateTest(test => ({...test, questions: questions}));
        setSelected(test.questions.length - 1);
    }

    const nextQuestion = () => {
        if (selected === test.questions.length - 1) {
            addNewQuestion();
        } else {
            setSelected(selected => selected + 1);
        }
    }

    const prevQuestion = () => {
        if (selected !== 0) {
            setSelected(selected => selected - 1);
        }
    }

    const input = useRef(null);

    function handleLoadImage() {


        if (input.current.files[0].size > 1024 * 1024 * 5) {
            const notification = buildNotification("IMAGE_SIZE", {}, dictionary);
            NotificationManager.error(notification.text, notification.summary, 4000);
        }

        const fd = new FormData();
        fd.append("file", input.current.files[0]);
        input.current.value = null;
        setImageLoading(true);
        http.post(keywords.IMAGES, fd).then(
            response => {
                setImageLoading(false);
                let questions = test.questions;
                questions[selected].imageId = response.data;
                updateTest(state => ({
                    ...state,
                    questions: questions
                }))
            }
        ).catch(
            error => {
                console.error(error);
                setImageLoading(false);
            }
        )
    }

    const [preview, setPreview] = useState(false);

    const handlePreview = () => {
        setPreview(true);
    }

    const clearImage = (e) => {
        e.stopPropagation();
        input.current.value = null;
        let questions = test.questions;
        questions[selected].imageId = null;
        updateTest(state => ({
            ...state,
            questions: questions
        }))
    };

    const [imageLoading, setImageLoading] = useState(false);

    return (
        <>
            <input style={{display: "none"}} type={"file"} ref={input} onChange={handleLoadImage} accept={"image/*"}/>
            <EditTestInfoButton onClick={() => setQuestions(false)}>
                {dictionary.EDIT_TEST_INFO}
            </EditTestInfoButton>
            <EditToolbox>
                {selected !== 0 ? <QuestionButtonLeft onClick={prevQuestion}/> : <div/>}
                <div>
                    <TestNameView>
                        <TestNameLabel>{test.name}</TestNameLabel>
                    </TestNameView>
                    <QuestionsEditingForm>
                        <QuestionInfo>
                            <QuestionTextFlexBox>
                                <QuestionTextContainer>
                                    <QuestionTextEditBox value={test.questions[selected].text}
                                                         placeholder={dictionary.QUESTION_TEXT}
                                                         onChange={event => {
                                                             event.persist();
                                                             setSelectedQuestionText(event.target.value)
                                                         }}/>
                                </QuestionTextContainer>
                            </QuestionTextFlexBox>
                            <PointsAndTypeFlexBox>
                                <PreviewOverlay open={preview} onClose={() => setPreview(false)}>
                                    <OverlayImg
                                        src={test.questions[selected].imageId ? keywords.IMAGES + "?id=" + test.questions[selected].imageId : null}
                                        alt={"Error"}/>
                                </PreviewOverlay>
                                <ImageContainer onClick={test.questions[selected].imageId ? handlePreview : null}>
                                    {
                                        test.questions[selected].imageId ?
                                            <>
                                                <Image
                                                    src={keywords.IMAGES + "?id=" + test.questions[selected].imageId}/>
                                                <ClearButton onClick={clearImage}/>
                                            </> : <ImagePlaceholder>
                                                {imageLoading ? dictionary.LOADING + "..." : dictionary.IMAGE}
                                            </ImagePlaceholder>
                                    }
                                </ImageContainer>
                                <MiddleContainer>
                                    <AddImageButton onClick={() => input.current.click()}>
                                        {dictionary.ADD_IMAGE}
                                    </AddImageButton>
                                    <div style={{flex: 1}}/>
                                    <PointsContainer>
                                        <InfoText>{dictionary.POINTS}</InfoText>
                                        <div style={{flex: 1}}/>
                                        <NumberInput type={"number"} value={test.questions[selected].points}
                                                     onChange={
                                                         event => {
                                                             setSelectedQuestionPoints(event.target.value)
                                                             event.persist();
                                                         }
                                                     }
                                                     onBlur={() => {
                                                         let questions = test.questions;
                                                         if (questions[selected].points === "" || parseInt(test.points) < 1) {
                                                             questions[selected].points = 1;
                                                             updateTest(state => ({
                                                                 ...state,
                                                                 questions: questions
                                                             }))
                                                         }
                                                     }}/>
                                    </PointsContainer>
                                </MiddleContainer>
                                <QuestionTypeSelector>
                                    <QuestionTypeRow>
                                        <ButtonInput
                                            checked={test.questions[selected].type === QuestionTypes.SINGLE_SELECT_QUESTION}
                                            type={"radio"}
                                            onClick={() => {
                                                setSelectedQuestionType(QuestionTypes.SINGLE_SELECT_QUESTION)
                                            }}/>
                                        <QuestionTypeLabel onClick={() => {
                                            setSelectedQuestionType(QuestionTypes.SINGLE_SELECT_QUESTION)
                                        }}>
                                            {dictionary.SINGLE_SELECT_QUESTION}
                                        </QuestionTypeLabel>
                                    </QuestionTypeRow>
                                    <QuestionTypeRow>
                                        <ButtonInput
                                            checked={test.questions[selected].type === QuestionTypes.MULTIPLE_SELECT_QUESTION}
                                            type={"radio"}
                                            onClick={() => {
                                                setSelectedQuestionType(QuestionTypes.MULTIPLE_SELECT_QUESTION)
                                            }}/>
                                        <QuestionTypeLabel onClick={() => {
                                            setSelectedQuestionType(QuestionTypes.MULTIPLE_SELECT_QUESTION)
                                        }}>
                                            {dictionary.MULTIPLE_SELECT_QUESTION}
                                        </QuestionTypeLabel>
                                    </QuestionTypeRow>
                                    <QuestionTypeRow>
                                        <ButtonInput
                                            checked={test.questions[selected].type === QuestionTypes.WRITING_QUESTION}
                                            type={"radio"}
                                            onClick={() => {
                                                setSelectedQuestionType(QuestionTypes.WRITING_QUESTION)
                                            }}/>
                                        <QuestionTypeLabel onClick={() => {
                                            setSelectedQuestionType(QuestionTypes.WRITING_QUESTION)
                                        }}>
                                            {dictionary.WRITING_QUESTION}
                                        </QuestionTypeLabel>
                                    </QuestionTypeRow>
                                </QuestionTypeSelector>
                            </PointsAndTypeFlexBox>
                        </QuestionInfo>
                        <AnswersList>
                            {test.questions[selected].answers.map(
                                (answer, index) => {
                                    const type = test.questions[selected].type;
                                    return (
                                        <AnswerRow key={selected * 10 + index}>
                                            {type === QuestionTypes.SINGLE_SELECT_QUESTION ?
                                                <ButtonInput checked={answer.isTrue} type={"radio"}
                                                             onClick={() => setTrue(index, !answer.isTrue)}/> :
                                                (type === QuestionTypes.MULTIPLE_SELECT_QUESTION ?
                                                    <ButtonInput checked={answer.isTrue} type={"checkBox"}
                                                                 onClick={() => setTrue(index, !answer.isTrue)}/> :
                                                    <div style={{height: "3vmin", width: "3vmin"}}/>)}
                                            <div style={{flex: 1}}/>
                                            <AnswerTextContainer>
                                                <AnswerText value={answer.text}
                                                            onChange={event => setAnswerText(index, event.target.value)}/>
                                            </AnswerTextContainer>
                                            <div style={{flex: 1}}/>
                                            {test.questions[selected].answers.length > 2 ||
                                            (test.questions[selected].answers.length > 1 &&
                                                test.questions[selected].type === QuestionTypes.WRITING_QUESTION)
                                                ? <DeleteAnswerButton onClick={() => deleteAnswer(index)}/> :
                                                <div style={{height: "3vmin", width: "3vmin"}}/>}
                                        </AnswerRow>
                                    )
                                }
                            )}
                            {test.questions[selected].answers.length < 8 ?
                                <AddAnswerButton onClick={() => addAnswer()}>
                                    {dictionary.ADD_ANSWER}
                                </AddAnswerButton>
                                : <></>}
                        </AnswersList>
                    </QuestionsEditingForm>
                </div>
                <QuestionButtonRight onClick={nextQuestion}/>
            </EditToolbox>
            <QuestionsList updateTest={updateTest} dictionary={dictionary} questions={test.questions}
                           setSelected={setSelected} test={test} save={save} selected={selected}
                           newQuestion={addNewQuestion}/>
        </>
    )
}

function QuestionsList({dictionary, test, updateTest, selected, setSelected, save, newQuestion}
) {

    const deleteQuestion = (index) => {
        let questions = test.questions;
        questions.splice(index, 1);
        if (selected >= questions.length) {
            setSelected(questions.length - 1);
        }
        updateTest(test => ({...test, questions}));
    }

    return (
        <QuestionListContainer>
            <QuestionListHeader>
                <InfoText style={{color: "white"}}>{dictionary.QUESTION_LIST}</InfoText>
            </QuestionListHeader>
            <QuestionListContent>
                {test.questions.map((question, index) => {
                    return (
                        <QuestionRow style={selected === index ? {background: "#D5D5D5"} : {}} key={index}
                                     onClick={() => setSelected(index)}>
                            <QuestionTextPreview>
                                {(index + 1) + ". " + question.text}
                            </QuestionTextPreview>
                            {test.questions.length > 1 ? <DeleteQuestionButton onClick={(event) => {
                                    deleteQuestion(index);
                                    event.stopPropagation();
                                }}/> :
                                <div/>}
                        </QuestionRow>
                    )
                })}
                <QuestionRow onClick={newQuestion}>
                    <QuestionTextPreview>{dictionary.ADD_QUESTION}</QuestionTextPreview>
                </QuestionRow>
            </QuestionListContent>
            <QuestionListFooter>
                <SaveButtonQuestionList onClick={save}>{dictionary.SAVE_TEST}</SaveButtonQuestionList>
            </QuestionListFooter>
        </QuestionListContainer>
    )
}


function TestInfo({test, updateTest, dictionary, setQuestions, save, setSelected}) {

    const history = useHistory();
    return (
        <>
            <TestInfoForm>
                <TestNameContainer>
                    <TestName value={test.name} onChange={event => {
                        event.persist();
                        updateTest(state => ({
                            ...state,
                            name: event.target.value
                        }))
                    }} placeholder={dictionary.TEST_NAME}/>
                </TestNameContainer>
                <MetaInfo>
                    <DurationField>
                        <InfoText>{dictionary.DURATION + ":"}</InfoText>
                        <div style={{flex: 1}}/>
                        <NumberInput type={"number"} value={test.duration} onChange={event => {
                            let value = event.target.value;
                            if (value > 0 || value === "") {
                                updateTest(state => ({
                                    ...state,
                                    duration: value
                                }))
                            } else {
                                updateTest(state => ({
                                    ...state,
                                    duration: MINIMUM_TEST_DURATION
                                }))
                            }
                            event.persist();
                        }} onBlur={() => {
                            if (test.duration === "" || parseInt(test.duration) < MINIMUM_TEST_DURATION) {
                                updateTest(state => ({
                                    ...state,
                                    duration: MINIMUM_TEST_DURATION
                                }))
                            }
                        }}/>
                    </DurationField>
                    <InfoText>
                        {dictionary.QUESTION_COUNT + ": " + test.questions.length}
                    </InfoText>
                    <InfoText>
                        {dictionary.MAX_POINTS + ": " + countPoints(test.questions)}
                    </InfoText>
                </MetaInfo>
                <TestDescriptionContainer>
                    <TestDescription value={test.description} onChange={event => {
                        event.persist();
                        updateTest(state => ({
                            ...state,
                            description: event.target.value
                        }))
                    }}/>
                </TestDescriptionContainer>
                <QuestionsButton onClick={() => {
                    setSelected(0);
                    setQuestions(true);
                }}>
                    <InfoText>{dictionary.QUESTIONS}</InfoText>
                </QuestionsButton>
            </TestInfoForm>
            <CancelButton onClick={() => history.goBack()}>
                {dictionary.CANCEL}
            </CancelButton>
            <SaveButton onClick={save}>
                {dictionary.SAVE_TEST}
            </SaveButton>
        </>
    )
}

export default connect(mapStateToProps)(TestEdit);