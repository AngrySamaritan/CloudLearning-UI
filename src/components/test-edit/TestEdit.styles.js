import styled from "styled-components";
import {DefaultH3, DefaultTextButton} from "../Common.styles";
import Popup from "reactjs-popup";

export const Loader = styled.div`
  background: white;
  border: 1.3vmin solid #9d959d;
  border-top: 1.3vmin solid #4a90e2;
  border-radius: 50%;
  width: 6vmin;
  height: 6vmin;
  animation: spin 1s linear infinite;

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`



export const LoadingOverlay = styled(Popup)`
  &-overlay {
    backdrop-filter: blur(5px);
    z-index: 5;
  }
`
export const ContentForm = styled.div`
  display: flex;
  flex-flow: column wrap;
  position: fixed;
  top: calc(68vh / 10.8);
  left: calc(389vw / 19.2);
  height: calc(956vh / 10.8);
  width: calc(1483vw / 19.2);
`

export const TestInfoForm = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  position: fixed;
  left: calc(488vw / 19.2);
  top: calc((135vh) / 10.8);
  height: calc(843vh / 10.8);
  width: calc(945vw / 19.2);
  background: #ffffff;
  border-radius: 2vmin;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
`

export const TestNameContainer = styled.div`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: center;
  margin-top: calc(50% / 8.43);
  height: calc(73% / 8.43);
  width: calc(848% / 9.58);
  border-radius: 2vmin;
  background: #2e2e2e;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.25);
`

export const TestName = styled.input`
  display: block;
  outline: none;
  border: none;
  background: none;
  line-height: 3.7vh;
  font-family: Arial, serif;
  font-size: 2.6vmin;
  color: #ffffff;
  border-bottom: #F77A52 solid 0.1vh;
  text-align: center;
`

export const MetaInfo = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: calc(10% / 8.43);
  width: calc(848% / 9.58);
  height: calc(55% / 8.43);
`

export const DurationField = styled.div`
  display: flex;
  flex-flow: row wrap;
  height: calc(32% / 0.55);
  width: calc(150% / 8.48);
  justify-content: center;
  align-items: center;
`

export const InfoText = styled.h3`
  cursor: default;
  font-family: Arial, serif;
  width: calc(198% / 8.48);
  font-style: normal;
  font-weight: normal;
  font-size: 1.85vmin;
  line-height: 1.85vmin;
  text-align: center;
`

export const NumberInput = styled(TestName)`
  line-height: 1.85vmin;
  font-size: 1.85vmin;
  width: 3vw;
  text-align: center;
  color: #15171e;
  border-bottom: #4a90e2 solid 0.2vh;
`

export const TestDescriptionContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: calc(848% / 9.58);
  height: calc(527% / 8.43);
  background: #ECECEC;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const TestDescription = styled.textarea`
  font-family: Arial, serif;
  width: 95%;
  height: 95%;
  font-style: normal;
  font-weight: normal;
  font-size: 2vmin;
  border: none;
  background: none;
  line-height: 1.85vmin;
  text-underline: #4a90e2;
  resize: none;
  outline: none;
`

export const QuestionsButton = styled.div`
  display: flex;
  position: relative;
  width: calc(848% / 9.58);
  height: calc(37% / 8.43);
  top: calc(34% / 8.43);
  background: #FFFFFF;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 2vmin;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  &:hover {
    background: #2e2e2e;
    color: white;
  }
`

export const Button = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  border: #15171e solid 1px;
  background: white;
  border-radius: 2vmin;

  &:hover {
    background: black;
    color: white;
  }
`

export const SaveButton = styled(Button)`
  position: fixed;
  bottom: calc(43vh / 10.8);
  top: auto;
  right: calc(474vw / 19.2);
  left: auto;
  height: calc(38vh / 10.8);
  width: calc(168vw / 19.2);
`

export const CancelButton = styled(SaveButton)`
  left: calc(481vw / 19.2);
  right: auto;
`

export const EditTestInfoButton = styled(DefaultTextButton)`
  display: flex;
  position: relative;
  justify-content: center;
  align-items: center;
  background: white;
  color: black;
  height: calc(37vh / 10.8);
  width: calc(168vw / 19.2);
  left: calc(872vw / 19.2);

  &:hover {
    background: black;
    color: white;
  }
`

export const EditToolbox = styled.div`
  position: relative;
  width: calc(1137vw / 19.2);
  height: calc(894vh / 10.8);
  left: 0;
  top: calc(23vh / 10.8);
`
export const TestNameView = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  top: 0;
  left: calc(94vw / 19.2);
  width: calc(945vw / 19.2);
  height: calc(42vh / 10.8);
`

export const TestNameLabel = styled.h3`
  cursor: default;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  width: calc(945vw / 19.2);
  height: calc(42vh / 10.8);
  font-size: 2.5vmin;
  line-height: 1.85vmin;
  border-radius: 2vmin;
  background: #2e2e2e;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.25);
`

export const QuestionsEditingForm = styled.div`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  top: calc(9vh / 10.8);
  left: calc(95vw / 19.2);
  height: calc(843vh / 10.8);
  width: calc(945vw / 19.2);
  background: #ffffff;
  border-radius: 2vmin;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
`
export const QuestionInfo = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  position: relative;
  margin-top: calc(26vh / 10.8);
  height: calc(278vh / 10.8);
  width: calc(861vw / 19.2);
  background: #ECECEC;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const QuestionTextFlexBox = styled.div`
  margin-top: calc(21vh / 10.8);
  flex: 1;
`

export const QuestionTextContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  left: calc(12vw / 19.2);
  height: calc(93vh / 10.8);
  width: calc(837vw / 19.2);
  border-radius: 2vmin;
  background: #2e2e2e;
`

export const QuestionTextEditBox = styled.textarea`
  height: 80%;
  width: 95%;
  background: #2e2e2e;
  resize: none;
  color: white;
  outline: none;
  border: none;
`

export const PointsAndTypeFlexBox = styled.div`
  flex: 1;
  display: flex;
  flex-flow: row;
  align-items: center;
  margin-bottom: calc(10vw / 19.2);
`

export const MiddleContainer = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  flex: 1;
  height: 100%;
`

export const AddImageButton = styled(DefaultTextButton)`
  width: calc(158vw / 19.2);
  height: calc(37vh / 10.8);
  border-radius: 2vh;
  font-size: 1rem;
  background: white;
`

export const PointsContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 1.85vmin;
`

export const ImageContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: calc(12vw / 19.2);
  width: calc(330vw / 19.2);
  height: calc(145vh / 10.8);
  background: #FFFFFF;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 2vmin;
`

export const Image = styled.img`
  height: 100%;
  border-radius: 2vmin;
  max-width: 100%;
  object-fit: cover;
`

export const ClearButton = styled.img`
  background: white;
  border: black solid 2px;
  position: absolute;
  cursor: pointer;
  right: 1rem;
  left: auto;
  top: 1rem;
  height: 1rem;
  content: url("/img/cancel.png");

  &:hover {
    //border: white solid 2px;
    background: black;
    content: url("/img/cancel_white_hover.png");
  }
`

export const PreviewOverlay = styled(Popup)`
  &-overlay {
    backdrop-filter: blur(5px);
    z-index: 5;
  }
`

export const OverlayImg = styled.img`
  max-height: 90%;
  max-width: 50%;
  border: black 2px solid;
  background: white;
  padding: 1vmin;
`

export const QuestionTypeSelector = styled.div`
  position: relative;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  margin-right: calc(12vw / 19.2);
  width: calc(330vw / 19.2);
  height: calc(145vh / 10.8);
  background: #FFFFFF;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 2vmin;
`

export const QuestionTypeRow = styled.div`
  display: flex;
  align-items: center;
  height: 3vh;
  width: 80%;
  margin-left: 10%;
`

export const QuestionTypeLabel = styled.h3`
  cursor: pointer;
  line-height: 1.85vmin;
  color: black;
  font-size: 1.85vmin;
  border-bottom: #4a90e2 solid 1px;
`

export const ButtonInput = styled.input`
  cursor: pointer;
  height: 1.5vmin;
  width: 1.5vmin;
  margin: calc(1.5vmin / 2);
`

export const ImagePlaceholder = styled(DefaultH3)`
  color: #9d959d;
`

export const AnswersList = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  width: calc(861vw / 19.2);
  height: calc(406vh / 10.8);
  margin-top: calc(38vh / 10.8);
  background: #ECECEC;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const AnswerRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: calc(10vh / 10.8);
  width: calc(825vw / 19.2);
  height: calc(38vh / 10.8);
`

export const AnswerTextContainer = styled.div`
  display: flex;
  align-items: center;
  width: calc(749vw / 19.2);
  height: calc(38vh / 10.8);
  background: #FFFFFF;
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 2vmin;
`

export const DeleteAnswerButton = styled.img`
  content: url("/img/delete.png");
  height: 3vmin;
  width: 3vmin;
  margin: 0;

  &:hover {
    content: url("/img/delete2.png");
  }
`

export const AnswerText = styled.input`
  line-height: 95%;
  margin-left: 0.5vw;
  width: calc(100% - 1vw);
  font-size: 1.85vmin;
  color: #15171e;
  border: none;
  outline: none;
  border-bottom: #4a90e2 solid 0.2vh;
`

export const AddAnswerButton = styled(Button)`
  width: calc(749vw / 19.2);
  height: calc(38vh / 10.8);
  margin-left: auto;
  margin-right: auto;
  margin-top: calc(10vh / 10.8);
`

export const QuestionButtonLeft = styled.img`
  position: absolute;
  cursor: pointer;
  left: 0;
  top: calc(50vh / 10.8);
  content: url("/img/button_left_white.png");

  &:hover {
    content: url("/img/button_left_black.png");
  }

`
// TODO: fix top value
export const QuestionButtonRight = styled(QuestionButtonLeft)`
  left: auto;
  right: 0;
  transform: scale(-1, 1);
`

export const QuestionListContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: calc(277vw / 19.2);
  height: calc(894vh / 10.8);
  margin-top: auto;

  background: #FFFFFF;
  box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.25);
  border-radius: 2vmin;
`

export const QuestionListHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 2vmin 2vmin 0 0;
  width: 100%;
  height: calc(53vh / 10.8);
  background: #2e2e2e;
`

export const QuestionListContent = styled.div`
  flex: 1;
  overflow-y: scroll;
`

export const QuestionRow = styled.div`
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  width: calc(245vw / 19.2);
  height: calc(34vh / 10.8);
  margin-left: auto;
  margin-right: auto;
  margin-top: calc(14vh / 10.8);
  border-radius: 2vmin;
  background: #ECECEC;
  box-shadow: 4px 4px 6px rgba(0, 0, 0, 0.25);

  &:hover {
    background: #E0E0E0;
  }
`

export const QuestionTextPreview = styled(InfoText)`
  cursor: pointer;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  margin-left: 2vh;
  width: 50%;
  height: 2vh;
`

export const DeleteQuestionButton = styled(DeleteAnswerButton)`
  height: 80%;
  z-index: 10;
`

export const SaveButtonQuestionList = styled(Button)`
  height: 30%;
  width: 60%;
`

export const QuestionListFooter = styled(QuestionListHeader)`
  border-radius: 0 0 2vmin 2vmin;
  height: calc(150vh / 10.8);
  background: white;
  border-top: black solid 2px;
`