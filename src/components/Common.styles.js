import styled from "styled-components";

export const DefaultForm = styled.div`
  display: flex;
  background-color: #fff;
  border-radius: 2vmin;
`

export const DefaultHeader = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  height: calc(79vh / 10.8);
  background: #2e2e2e;
  border-radius: 2vh 2vh 0 0;
`

export const DefaultFooter = styled(DefaultHeader)`
  width: 100%;
  background: #2e2e2e;
  height: calc(15vh / 10.8);
  border-radius: 0 0 2vmin 2vmin;
`

export const DefaultH3 = styled.h3`
  position: relative;
  cursor: default;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: calc(32vmin / 10.8);
  line-height: calc(32vmin / 10.8);
  display: flex;
  align-items: center;
  color: #ffffff;
`;

export const DefaultH4 = styled.h4`
  font-size: calc(24vmin / 10.8);
  line-height: calc(24vmin / 10.8);
`

export const DefaultTextButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 2vmin;
  line-height: 2vh;
  text-align: center;
  
  width: calc(212vw / 19.2);
  height: calc(37vh / 10.8);
  color: black;
  border-radius: 2vmin;
  border: black 1px solid;
  &:hover {
    background: black;
    color: white;
  }
`