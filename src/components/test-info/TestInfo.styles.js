import styled from "styled-components"
import {DefaultForm, DefaultH4} from "../Common.styles";
import {Link} from "react-router-dom";

export const MainForm = styled(DefaultForm)`
  cursor: default;
  position: fixed;
  display: flex;
  flex-flow: column nowrap;
  top: 50px;
  left: 21%;
  width: 76%;
  height: 90%;
  background-color: #fff;
  opacity: 1;
  border-radius: 2vh;
  -webkit-border-radius: 2vh;
`

export const Clocks = styled.div`
  color: #F77A52;
  font-family: Arial, serif;
  font-size: 1.6vmin;

  position: absolute;
  margin-left: 1.5em;
  margin-top: 1.5em;
  width: 5em;
  height: 5em;
  border: 2px solid #4a90e2;
  border-radius: 50%;
  line-height: 5em; 
  text-align: center;
`

export const InfoRow = styled.div`
  font-family: Arial, serif;
  font-size: 2vmin;

  display: flex;
  flex-flow: row;
  margin-left: 2.5%;
  align-items: flex-end;
  justify-content: center;
  min-height: 12vh;
  width: 95%;
  border-bottom: #4a90e2 2px solid;
  padding-bottom: 0.522vh;
`

export const InfoText = styled(DefaultH4)`
  margin-bottom: 1vh;
`

export const StartButton = styled(Link)`
  width: calc(150vw / 19.2);
  height: calc(40vh / 10.8);
  font-family: Arial, serif;
  font-size: 2vmin;
  background: transparent;
  color: #454545;
  border: 2px solid #4a90e2;
  cursor: pointer;
  margin-left: auto;
  margin-right: auto;
  margin-top: 5vh;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  
  
  &:hover {
    border-color: #F77A52;
    color: #F77A52;
  }
`