import React, {useEffect, useState} from "react"
import {Clocks, InfoRow, InfoText, MainForm, StartButton} from "./TestInfo.styles";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import {connect} from "react-redux";

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
})

const TestInfo = ({match: {params: {id}}, dictionary}) => {

    const [test, setTest] = useState(
        {
            id: -1,
            duration: 0,
            name: dictionary.LOADING,
            description: dictionary.LOADING,
        }
    );

    useEffect(() => {
        http.get(keywords.TESTS + "/" + id + keywords.BRIEF).then(
            response => setTest(response.data)
        ).catch(
            error => console.error(error)
        )
    }, [id]);


    return <MainForm>
        <Clocks>{test.duration  + " " + dictionary.MINUTES}</Clocks>
        <InfoRow>
            <InfoText>{test.name}</InfoText>
        </InfoRow>
        <InfoRow>
            <InfoText>{test.description}</InfoText>
        </InfoRow>
        {id !== -1 ? <StartButton to={"/test/" + test.id}>{dictionary.START_TEST}</StartButton> : <div/>}
    </MainForm>
}

export default connect(mapStateToProps)(TestInfo);