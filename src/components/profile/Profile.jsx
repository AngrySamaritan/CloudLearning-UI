import React, {useEffect, useRef, useState} from "react";
import {
    AdminMenuBody,
    AdminMenuBodyPH,
    AdminOptionsButton,
    ButtonsContainer,
    CancelButton,
    ContentForm,
    DropZoneContainer,
    EditButton,
    EditInput,
    EditProfileImage,
    EditProfileOption,
    MainForm,
    Mark,
    OkButton,
    ProfileImage,
    ProfileImageContainer,
    ResultDate,
    ResultRow,
    ResultRowLink,
    ResultsList,
    SubjectAssigneeImage,
    SubjectAssigneeOption,
    TestName,
    TestResultsTitle,
    Text,
    TextField,
    TextFieldLabel,
    TextInfo,
    TextSubject,
} from "./Profile.styles";
import updateProfileCreator from "../../store/actionCreators/updateProfileCreator";
import {connect} from "react-redux";
import http from "../../util/Http";
import {keywords} from "../../config/KeyWords";
import {DropzoneArea} from "material-ui-dropzone";
import {Link, useHistory} from "react-router-dom";
import {hasRole} from "../../util/ProfileUtils";
import SubjectAssignee from "../subject-assignee-popup/SubjectAssignee";
import {PopUp} from "../subject-assignee-popup/SybjectAssignee.styles";

const profileUtils = require("../../util/ProfileUtils");

const mapDispatchToProps = dispatch => ({
    update_profile: (profile) => dispatch(updateProfileCreator(profile)),
});

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
    profile: state.profile,
    viewingProfile: state.viewingProfile,
    dummy: state.updateDummyFlag,
});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const LOADING_FLAG = "LOADING";

const IMAGE_UPLOAD_WAITING_SECONDS = 10;

function Profile({match: {params: {id}}, ...props}) {

    const [state, setState] =
        useState({
            editingProfile: {
                firstName: "",
                lastName: "",
                imageId: null,
                group: ""
            },
            viewingProfile: {
                id: "",
                username: "loading",
                firstName: "loading",
                lastName: "loading",
                email: "",
                group: "",
                imageId: null,
                groupId: "",
                roles: [],
                assignedSubjects: [{id: 0, name: "OOP"}, {id: 1, name: "DB"}, {id: 1, name: "DB"}],
            }
        });

    const [updating, setUpdating] = useState(false);

    const [isEditing, setEditing] = useState(false);

    const [menu, setMenu] = useState(false);

    const [subjectAssignee, setSubjectsAssignee] = useState(false);

    const isOwner = id.toString() === props.profile.id.toString();

    const setUsersProfile = props.update_profile;

    const history = useHistory();

    useEffect(() => {
        http.get(keywords.USERS + "/" + id).then(
            (response) => {
                setState(state => ({
                    ...state,
                    viewingProfile: {...state.viewingProfile, ...response.data},
                    editingProfile: response.data
                }));
                setUpdating(false);
            }
        ).catch(reason => {
            history.push("/home");
            console.log(reason);
        })
    }, [id, isOwner, isEditing, setUsersProfile, props.profile, updating])


    const startEditing = () => {
        setEditing(true);
    }

    const ok = async () => {
        let retries = 0;
        while (state.editingProfile.imageId === LOADING_FLAG && retries <= IMAGE_UPLOAD_WAITING_SECONDS) {
            await sleep(1000);
            retries++;
        }

        http.patch(keywords.USERS + "/" + id, state.editingProfile).then(() => {
            if (isOwner) {
                setUsersProfile({...props.profile, ...state.editingProfile});
            }
            setEditing(false);
        }).catch(reason => {
            console.log(reason);
        })
    }

    const cancel = () => {
        setEditing(false);
    }

    const SubMenu = () => {
        return menu ?
            <AdminMenuBody>
                <EditProfileOption onClick={() => setEditing(true)}>
                    <EditProfileImage/>
                    {props.dictionary.EDIT_PROFILE}
                </EditProfileOption>
                <SubjectAssigneeOption style={{borderBottom: "none"}} onClick={() => setSubjectsAssignee(true)}>
                    <SubjectAssigneeImage/>
                    {props.dictionary.ASSIGNEE_SUBJECT}
                </SubjectAssigneeOption>
            </AdminMenuBody> :
            <AdminMenuBodyPH/>
    }

    return (
        <MainForm>
            <PopUp open={subjectAssignee} modal onClose={() => setSubjectsAssignee(false)}>
                {close => <SubjectAssignee open={subjectAssignee} close={close} userId={id}
                                           dictionary={props.dictionary} updating={() => setUpdating(true)}/>}
            </PopUp>
            <TestResultsForm dictionary={props.dictionary} id={id} isOwner={isOwner}/>
            <div style={{flex: 30}}/>
            <ContentForm>
                {!isEditing ?
                    <ButtonsContainer>
                        <div style={{flex: 1}}/>
                        {isOwner ?
                            <EditButton onClick={startEditing}/>
                            :
                            hasRole(props.profile, keywords.ROLE_ADMIN) ?
                                <AdminOptionsButton menu={menu} onClick={() => setMenu(menu => !menu)}/>
                                :
                                <div/>}
                    </ButtonsContainer>
                    :
                    <ButtonsContainer>
                        <CancelButton onClick={cancel}/>
                        <div style={{flex: 1}}/>
                        <OkButton onClick={ok}/>
                    </ButtonsContainer>
                }
                <ProfileImageContainer>
                    {!isEditing ?
                        <ProfileImage src={state.viewingProfile.imageId ?
                            keywords.IMAGES + "?id=" + state.viewingProfile.imageId :
                            "/img/profileImage.png"}/>
                        :
                        <DropZoneContainer>
                            <DropzoneArea acceptedFiles={['image/*']}
                                          dropzoneText={props.dictionary.DRAG_IMG}
                                          showPreviewsInDropzone={true}
                                          filesLimit={1}
                                          onDrop={
                                              files => {
                                                  const fd = new FormData();
                                                  fd.append("file", files[0]);
                                                  http.post(keywords.IMAGES, fd).then(
                                                      (response) => {
                                                          setState({
                                                              ...state,
                                                              editingProfile: {
                                                                  ...state.editingProfile,
                                                                  imageId: response.data,
                                                              }
                                                          });
                                                      }
                                                  ).catch(reason => {
                                                      console.log(reason);
                                                  })
                                              }
                                          }
                                // getPreviewIcon={file => <DropZonePreview src={URL.createObjectURL(file)}/>}
                            />
                        </DropZoneContainer>
                    }
                </ProfileImageContainer>
                <TextInfo>
                    <TextField>
                        <Text>{state.viewingProfile.username}</Text>
                        <TextFieldLabel>{props.dictionary.USERNAME}</TextFieldLabel>
                    </TextField>
                    <TextField>
                        {!isEditing ?
                            <Text>{state.viewingProfile.firstName}</Text>
                            :
                            <EditInput value={state.editingProfile.firstName} onChange={
                                (e) => {
                                    setState({
                                        ...state,
                                        editingProfile: {
                                            ...state.editingProfile,
                                            firstName: e.target.value,
                                        }
                                    })
                                }}/>
                        }
                        <TextFieldLabel>{props.dictionary.FIRST_NAME}</TextFieldLabel>
                    </TextField>
                    <TextField>
                        {!isEditing ?
                            <Text>{state.viewingProfile.lastName}</Text>
                            :
                            <EditInput value={state.editingProfile.lastName} onChange={
                                (e) => {
                                    setState({
                                        ...state,
                                        editingProfile: {
                                            ...state.editingProfile,
                                            lastName: e.target.value,
                                        }
                                    })
                                }}/>}
                        <TextFieldLabel>{props.dictionary.LAST_NAME}</TextFieldLabel>
                    </TextField>
                    <TextField>
                        <Text>{state.viewingProfile.email}</Text>
                        <TextFieldLabel>{props.dictionary.EMAIL}</TextFieldLabel>
                    </TextField>
                    <TextField>
                        {!isEditing ?
                            <Link
                                to={"/group/" + state.viewingProfile.groupId}><Text>{state.viewingProfile.group}</Text></Link>
                            :
                            <EditInput value={state.editingProfile.group} onChange={
                                (e) => {
                                    setState({
                                        ...state,
                                        editingProfile: {
                                            ...state.editingProfile,
                                            group: e.target.value,
                                        }
                                    })
                                }}/>}
                        <TextFieldLabel>{props.dictionary.GROUP}</TextFieldLabel>
                    </TextField>
                    {

                        profileUtils.hasRole(state.viewingProfile, keywords.ROLE_MODER) &&
                        !profileUtils.hasRole(state.viewingProfile, keywords.ROLE_ADMIN) ?
                            <TextField>
                                <TextSubject>
                                    {

                                        state.viewingProfile.assignedSubjects.map((subject, index) => {
                                            return (
                                                <Link
                                                    to={`/subject/id${subject.id}`}>{subject.name + (index === state.viewingProfile.assignedSubjects.length - 1 ? "" : ",")}</Link>
                                            )
                                        })
                                    }
                                </TextSubject>
                                <TextFieldLabel>{props.dictionary.ASSIGNED_SUBJECTS}</TextFieldLabel>
                            </TextField> : <div/>
                    }
                </TextInfo>
            </ContentForm>
            <SubMenu/>
        </MainForm>
    );
}


const TestResultsForm = (
    {
        dictionary, id, isOwner
    }
) => {

    const [loaded, setLoaded] = useState(false)

    let results = useRef([
        {
            id: null,
            testName: null,
            completionDate: null,
            mark: null,
        },
    ]);


    useEffect(() => {
        setLoaded(false);
        http.get(keywords.USERS + keywords.RESULTS + "/" + id).then((response) => {
                results.current = response.data;
                setLoaded(true);
            }
        )
    }, [id])


    function ResultRowView({index, result, isOwner}) {
        return (
            isOwner ?
                <ResultRowLink key={index} to={"/test/result/id" + result.id}>
                    <TestName>{result.testName}</TestName>
                    <div style={{flex: 0.5}}/>
                    <Mark>{dictionary.MARK}:{result.mark}</Mark>
                    <div style={{flex: 0.5}}/>
                    <ResultDate>{result.completionDate}</ResultDate>
                </ResultRowLink>
                :
                <ResultRow key={index}>
                    <TestName>{result.testName}</TestName>
                    <div style={{flex: 0.5}}/>
                    <Mark>{dictionary.MARK}:{result.mark}</Mark>
                    <div style={{flex: 0.5}}/>
                    <ResultDate>{result.completionDate}</ResultDate>
                </ResultRow>
        )
    }

    return (
        <ContentForm>
            <TestResultsTitle>
                {dictionary.TEST_RESULTS}
            </TestResultsTitle>
            <ResultsList>
                {loaded ? results.current.map(
                    (result, index) => {
                        return (
                            <ResultRowView index={index} result={result} isOwner={isOwner}/>
                        )
                    }) : <div/>
                }
            </ResultsList>
        </ContentForm>
    )
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
