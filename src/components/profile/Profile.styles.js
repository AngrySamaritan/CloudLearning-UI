import styled from "styled-components";
import {Link} from "react-router-dom";

export const MainForm = styled.div`
  display: flex;
  flex-flow: row nowrap;
  position: fixed;
  top: 5.2vh;
  left: 20.22vw;
  width: calc(100vw - 20.22vw * 2 + 266vw / 19.2);
  height: 90%;
  opacity: 1;
  -webkit-border-radius: 20px;
`

export const ContentForm = styled.div`
  display: flex;
  flex-flow: column nowrap;
  flex: 555;
  height: 100%;
  background-color: #fff;
  opacity: 1;
  -webkit-border-radius: 20px;
`

export const ProfileImageContainer = styled.div`
  position: relative;
  top: -9vh;
  display: flex;
  justify-content: center;
  flex-direction: row;
`

export const ProfileImage = styled.img`
  margin-top: 1rem;
  border-radius: 50%;
  border: 0 solid black; /* Параметры рамки */
  box-shadow: 0 0 7px #666; /* Параметры тени */
  height: 25.828vh;
  width: 25.828vh;
`

export const TextInfo = styled.div`
  position: relative;
  top: -9vh;
  display: flex;
  flex-flow: column wrap;
  justify-content: center;
`

export const TextField = styled.h3`
  display: block;
  position: relative;
  top: 7vh;
  margin: 4.62vh auto 0;
  width: 70%;
`

export const Text = styled.h3`
  display: block;
  outline: none;
  border: none;
  width: 100%;
  line-height: 3.7vh;
  font-family: Arial, serif;
  font-size: 2.6vh;
  color: #9d959d;
  border-bottom: 2px solid #4a90e2;
  cursor: default;
`

export const TextSubject = styled.h3`
  display: block;
  outline: none;
  border: none;
  width: 100%;
  line-height: 3.7vh;
  font-family: Arial, serif;
  font-size: 2.6vh;
  color: #9d959d;
  border-bottom: 2px solid #4a90e2;
  cursor: default;
  display: flex;
  overflow-x: auto;
  text-overflow: ellipsis;
  
  a:hover {
    color: black;
  }
  
  a {
    margin-right: 5px;
  }
    
`

export const TextFieldLabel = styled.label`
  position: absolute;
  left: 0;
  top: 10px;
  color: #F77A52;
  font-family: Arial, serif;
  font-size: 16px;
  font-weight: 300;
  transform: translateY(-35px);
  transition: all 0.2s ease;
`

export const Button = styled.img`
  height: 4vh;
  width: 4vh;
  padding: 5vh 5vh 0;
  z-index: 2;
`

export const AdminOptionsButton = styled(Button)`
  content: ${props => !props.menu ? 'url("/img/menu_button.png")' : 'url("/img/menu_button_hover.png")'};
  padding: calc(15vh / 10.8) calc(2vw / 19.2) 0 0;


  &:hover {
    content: url("/img/menu_button_hover.png");
  }
`

export const EditButton = styled(Button)`
  content: url("/img/edit.png");
  padding: 5vh 5vh 0 0;


  &:hover {
    content: url("/img/edit2.png");
  }
`

export const CancelButton = styled(Button)`
  position: relative;
  content: url("/img/cancel.png");

  &:hover {
    content: url("/img/cancel2.png");
  }
`

export const OkButton = styled(Button)`
  position: relative;
  content: url("/img/ok.png");

  &:hover {
    content: url("/img/ok2.png");
  }
`

export const ButtonsContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  height: 9vh;
`
export const DropZoneContainer = styled.div`
  margin-top: 1rem;
  //border-radius: 50%;
  border: 0 solid black; /* Параметры рамки */
  box-shadow: 0 0 7px #666; /* Параметры тени */
  height: 25.828vh;
  width: 25.828vh;
`

export const EditInput = styled.input`
  display: block;
  outline: none;
  border: none;
  width: 100%;
  line-height: 3.7vh;
  font-family: Arial, serif;
  font-size: 2.6vh;
  color: #9d959d;
  border-bottom: 2px solid #4a90e2;
`

export const TestResultsTitle = styled.div`
  display: block;
  position: relative;
  top: 5.14px;
  width: 100%;
  height: 8.7vh;
  line-height: 12.7vh;
  font-family: Arial, serif;
  font-size: 3vh;
  text-align: center;
  vertical-align: bottom;
  color: #9d959d;
  border-bottom: 2px solid #4a90e2;
  cursor: default;
`

export const ResultsList = styled.div`
  position: relative;
  top: 6.5%;
  left: 4.28%;
  width: 91.42%;
  height: 75%;
  background-color: #Fff;
  opacity: 1;
  overflow: auto;
  box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
`

export const ResultRow = styled.div`
  position: relative;
  display: flex;
  flex-flow: row;
  left: 10%;
  margin-top: 4.1%;
  color: #2e2e2e;
  width: 80%;
  cursor: default;
  text-decoration: none;
  font-family: Arial, serif;
  font-size: 2vh;
`

export const ResultRowLink = styled(Link)`
  position: relative;
  display: flex;
  flex-flow: row;
  left: 10%;
  width: 80%;
  margin-top: 4.1%;
  color: #2e2e2e;
  justify-content: center;
  text-decoration: none;
  font-family: Arial, serif;
  font-size: 2vm;

  &:hover {
    color: #aaaaaa;
  }
`

export const ResultText = styled.h3`
    text-overflow: ellipsis;
`

export const TestName = styled(ResultText)`
    width: 35%;
    overflow: hidden;
`

export const ResultDate = styled(ResultText)`
`

export const Mark = styled(ResultText)`
`

export const AdminMenuBody = styled.div`
  position: relative;
  display: flex;
  flex-flow: column nowrap;
  width: calc(251vw / 19.2);
  height: calc(122vh / 10.8);
  left: calc(15vw / 19.2);

  background: #FFFFFF;
  border-radius: 2vmin;
`

export const AdminMenuOption = styled.div`
  flex: 1;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  cursor: pointer;
  font-family: Arial, serif;
  font-style: normal;
  font-weight: normal;
  font-size: 2vmin;
  line-height: 2vh;
  border-bottom: black 2px solid;
`

export const OptionImage = styled.img`
  height: calc(35vmin / 10.8);
  width: calc(35vmin / 10.8);
  margin-right: calc(10vw / 19.2);

`

export const EditProfileOption = styled(AdminMenuOption)``;
export const SubjectAssigneeOption = styled(AdminMenuOption)``;

export const EditProfileImage = styled(OptionImage)`
  content: url("/img/edit.png");

  ${EditProfileOption}:hover & {
    content: url("/img/edit2.png");
  }
`

export const SubjectAssigneeImage = styled(OptionImage)`
  content: url("/img/subject_assignee.png");

  ${SubjectAssigneeOption}:hover & {
    content: url("/img/subject_assignee_hover.png");
  }
`

export const AdminMenuBodyPH = styled.div`
  position: relative;
  width: calc(251vw / 19.2);
  height: calc(122vh / 10.8);
  left: calc(15vw / 19.2);
`