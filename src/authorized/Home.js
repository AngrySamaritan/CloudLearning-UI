import * as React from "react";
import styles from "./main.module.css";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

const mapStateToProps = (state) => ({
    profile: state.profile
})

function Home({profile}) {

    return (
        profile.id === -1 ?
            <div className={styles.mainForm}>
                <img className={styles.logo2} src={process.env.PUBLIC_URL + "/img/logo2.png"}
                     alt="logo"/>
                <div className={styles.inDeveloping}>In developing...</div>
            </div> :
            <Redirect to={"/profile/" + profile.id}/>

    );
}

export default connect(mapStateToProps)(Home)