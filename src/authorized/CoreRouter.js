import * as React from 'react';
import AuthenticatedRoute from '../util/AuthenticatedRoute';
import {Redirect, Switch} from 'react-router-dom';
import Profile from '../components/profile/Profile';
import Subject from './Subject';
import LeftSideMenu from '../components/left-side-menu/LeftSideMenu';
import Group from '../components/group/Group';
import {TestRouter} from "../test/TestRouter";
import AdminRouter from "../routers/AdminRouter";
import Messages from "../components/messages/Messages";
import Home from "./Home";
import {TestCompletingComponent} from "../test/testCompleting/TestCompletingComponent";

export function CoreRouter() {
    if (localStorage.getItem("Authentication") !== "true") {
        return <Redirect to={"/login"}/>
    }


    return (
        <div>
            <LeftSideMenu/>
            <Switch>
                <AuthenticatedRoute exact path="/dev/:id" component={TestCompletingComponent}/>
                <AuthenticatedRoute exact path="/main" component={Home}/>
                <AuthenticatedRoute exact path={"/messenger"} component={Messages}/>
                <AuthenticatedRoute exact path={"/profile/:id"} component={Profile}/>
                <AuthenticatedRoute exact path="/subject/id:id" component={Subject}/>
                <AuthenticatedRoute exact path="/group/:id" component={Group}/>
                <AuthenticatedRoute path="/test/" component={TestRouter}/>
                <AuthenticatedRoute path={"/admin/"} component={AdminRouter}/>
                <AuthenticatedRoute path={"/"} component={Home}/>
            </Switch>
        </div>
    );
}

