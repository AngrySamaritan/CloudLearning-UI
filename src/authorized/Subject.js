import * as React from "react";
import {useEffect, useState} from "react";
import styles from "./subject.module.css";
import http from "../util/Http";
import {Link} from "react-router-dom";
import {keywords} from "../config/KeyWords";
import {isEqual} from "lodash";
import {connect} from "react-redux";
import {hasRole} from "../util/ProfileUtils";

const mapStateToProps = (state) => ({
    dictionary: state.dictionary,
    profile: state.profile
});

function Subject({history, profile, dictionary, ...props}) {

    const [subject, setSubject] = useState({
            id: 0,
            name: "",
            teachers: [
                {
                    firstName: "",
                    group: {
                        code: "",
                        id: 0
                    },
                    id: 0,
                    lastName: ""
                }
            ],
        }
    )

    const [allowedTests, setAllowedTests] = useState([
        {
            description: "",
            duration: 0,
            id: 0,
            name: "",
            author: {
                id: 0,
            }
        }
    ])

    useEffect(() => {
        http.get("/subject/id" + props.match.params.id).then(
            (response) => {
                setSubject(() => (response.data))
            }
        );
    }, [props.match.params.id]);


    useEffect(() => {
        http.get("/subject/id" + props.match.params.id + "/getAllowedTests").then(
            (response) => {
                setAllowedTests(response.data);
            }
        )
    }, [props.match.params.id])

    const CreateButton = () => {
        return (subject.teachers.some(user => user.id === profile.id) || hasRole(profile, keywords.ROLE_ADMIN) ?
            <div className={styles.create}>
                <button className={styles.button} onClick={
                    (event) => {
                        event.preventDefault();
                        history.push("/test/create/" + subject.id);
                    }
                }>{dictionary.CREATE_TEST}
                </button>
            </div> : <div/>);
    }

    const SubjectName = () => {
        return (
            <div className={styles.nameForm}>
                <form action="" className={styles.uiForm}>
                    <div className={styles.textForm}>
                        <div className={styles.subjectName} id={"name"}>{subject.name}</div>
                        <label htmlFor="name">{dictionary.SUBJECT}</label>
                        <CreateButton/>
                    </div>
                </form>
            </div>
        );
    }

    const Teachers = () => {
        return (

            <div className={styles.teacherForm}>
                <label>{dictionary.TEACHER_LIST}</label>
                <TeachersListView/>
            </div>
        );
    }

    const TeachersListView = () => {
        return subject.teachers.map(
            (user, index) => {
                return (
                    <Link key={index * -1} to={"/profile/" + user.id}>
                        <div className={styles.teacher}>
                            {user.firstName + " " + user.lastName}
                        </div>
                    </Link>);
            }
        );
    }


    const TestForm = () => {
        return (
            <div className={styles.testForm}>
                <label>{dictionary.TEST_LIST}</label>
                <TestListView/>
            </div>
        );

    }

    const ManageButton = (props) => {
        if (subject.teachers.some(teacher => teacher.id === profile.id)  || hasRole(profile, keywords.ROLE_ADMIN)) {
            return (
                <Link to={"/test/id" + props.test.id + "/management"}>
                    <img className={styles.settings} src={process.env.PUBLIC_URL + "/img/settings.png"} alt="settings"/>
                </Link>);
        } else {
            return (<div/>)
        }
    }

    const EditButton = (props) => {
        if (props.test.author.id === profile.id  || hasRole(profile, keywords.ROLE_ADMIN)) {
            return (
                <Link to={"/test/" + props.test.id + "/edit"}>
                    <img className={styles.edit} src={process.env.PUBLIC_URL + "/img/editTest.png"} alt="edit"/>
                </Link>);
        } else {
            return (<div/>)
        }
    }


    const DeleteButton = (props) => {
        if (props.test.author.id === profile.id || hasRole(profile, keywords.ROLE_ADMIN)) {
            return (
                <div onClick={() => {
                    http.delete(keywords.TESTS + "/" + props.test.id
                    ).then(() => {
                        setAllowedTests(allowedTests => (allowedTests.filter(test => !isEqual(test, props.test))));
                    });
                }}>
                    <img className={styles.delete} src={process.env.PUBLIC_URL + "/img/delete.png"} alt="delete"/>
                </div>);
        } else {
            return (<div/>)
        }
    }

    const TestListView = () => {
        return allowedTests.map(
            (test, index) => {
                return (
                    <div key={index}>
                        <div className={styles.test}>
                            {test.name}
                            <div className={styles.buttonGroupDiv}>
                                <EditButton test={test}/>
                                <DeleteButton test={test}/>
                                <ManageButton test={test}/>
                                <Link to={"/test/" + test.id + "/info"}>
                                    <img className={styles.start} src={process.env.PUBLIC_URL + "/img/start.png"}
                                         alt="start"/>
                                </Link>
                            </div>
                        </div>

                    </div>
                )
            }
        );
    }

    return (
        <div>
            <SubjectName/>
            <Teachers/>
            <TestForm/>
        </div>
    );


}

export default connect(mapStateToProps)(Subject)