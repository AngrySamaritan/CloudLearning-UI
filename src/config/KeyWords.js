export const keywords = {
	TESTS: '/tests',
	MANAGEMENT: '/management',

	CHECK: '/check',
	BRIEF: '/brief',
	USERS: '/users',
	ME: '/me',
	IMAGES: '/images',
	RESULTS: '/results',
	SUBJECTS: '/subjects',

	GROUPS: '/groups',
	FULL: '/full',
	SEARCH: '/search',

	ROLE_ADMIN: 'ROLE_ADMIN',
	ROLE_MODER: 'ROLE_MODER',

	WEBSOCKET: '/ws',

	CONFIRM: '/confirm',

    NOTIFICATIONS: "/notifications",
    USER: "/user",

	CHATS: "/chats",
	MESSAGES: "/messages"
};