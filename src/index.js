import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import configureStore from "./store/store";

require('velocity-animate')
require('velocity-animate/velocity.ui')
require('velocity-react/velocity-component')


ReactDOM.render(
    <BrowserRouter>
        <Provider store={configureStore()}>
            <App/>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
)
;

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
