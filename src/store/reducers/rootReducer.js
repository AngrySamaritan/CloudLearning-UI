import {setLanguageAction} from "../actions/setLanguageAction";
import {updateProfileAction} from "../actions/updateProfileAction";
import {forceUpdateAction} from "../actions/forceUpdateAction";

export default (state, action) => {
    switch (action.type) {
        case setLanguageAction.type:
            return {
                ...state,
                dictionary: action.value
            };
        case updateProfileAction.type:
            return {
                ...state,
                profile: action.value
            };
        case forceUpdateAction.type:
            return {
                ...state,
                updateDummyFlag: !state.updateDummyFlag,
            }
        default:
            return state;
    }
}