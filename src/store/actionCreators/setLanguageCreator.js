import {setLanguageAction} from "../actions/setLanguageAction";

const setLanguage = (state) => {
    return {
        type: setLanguageAction.type,
        value: state
    }
}

export default setLanguage;