import {forceUpdateAction} from "../actions/forceUpdateAction";

const forceUpdateCreator = () => {
    return {
        type: forceUpdateAction.type,
    }
}

export default forceUpdateCreator;