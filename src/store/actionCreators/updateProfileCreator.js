import {updateProfileAction} from "../actions/updateProfileAction";

const updateProfile = (profile) => {
    return {
        type: updateProfileAction.type,
        value: profile,
    }
}

export default updateProfile;