import {EN} from "../Dictionaries/EN";

export const initialState = {
    dictionary: EN,
    profile: {
        id: -1,
        username: "loading",
        firstName: "loading",
        lastName: "loading",
        email: "",
        group: "",
        imageId: "",
        imageUrl: "",
        roles: ["LOADING"]
    },
    updateDummyFlag: false.toString()
}