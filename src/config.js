export default class config {
    static serverUrl = "http://localhost:8080";
    static url = "http://localhost:3000/";
    static confirmationUrl = config.url + "confirm/";
    static passwordRestoreUrl = config.url + "restore_pass/";
    static redirectLoginUrl = "redirectLoginUrl";
}