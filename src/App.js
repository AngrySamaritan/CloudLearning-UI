import React from 'react';
import "./icons.css"
import {Route, Switch} from "react-router-dom";
import {UnauthenticatedRoute} from "./util/UnauthenticatedRoute";
import Main from "./indexPage/Main";
import Login from "./indexPage/login/Login";
import PasswordReset from "./indexPage/passwordrecovery/PasswordReset";
import NewPassword from "./indexPage/passwordrecovery/NewPassword";
import SignUp from "./indexPage/signup/SignUp";
import AuthService from "./util/AuthServise";
import {CoreRouter} from "./authorized/CoreRouter";
import {ConfirmComponent} from "./indexPage/ConfirmComponent";
import {connect} from "react-redux"
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


function App() {

    sessionStorage.setItem("beforeLoginRedirect", "/");


    return (
        <div>
            <Switch>
                <Route exact path={"/login"}>
                    <UnauthenticatedRoute/>
                    <Login/>
                </Route>
                <Route exact path="/">
                    <UnauthenticatedRoute/>
                    <Main/>
                </Route>
                <Route exact path="/logout">
                    <Logout/>
                </Route>
                <Route exact path="/sign_up">
                    <UnauthenticatedRoute/>
                    <SignUp/>
                </Route>
                <Route exact path="/password_recovery">
                    <UnauthenticatedRoute/>
                    <PasswordReset/>
                </Route>
                <Route exact path="/restore_pass/:uid" component={NewPassword}/>
                <Route exact path={"/confirm/:confirmUid"} component={ConfirmComponent}/>
                <Route path="/" component={CoreRouter}/>
            </Switch>
            <NotificationContainer/>
        </div>
    );
}

function Logout() {
    AuthService.logout();
    return <></>
}


export default connect()(App);

