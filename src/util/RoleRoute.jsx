import React from "react";
import {connect} from "react-redux";
import {hasRole} from "./ProfileUtils"
import {Redirect, Route} from "react-router-dom";

const mapStateToProps = (state) => ({
    profile: state.profile
})

const RoleRoute = ({profile, role, ...props}) => {
    return hasRole(profile, role) ? <Route {...props}/> : hasRole(profile, "LOADING") ? <div/> : <Redirect to={"/"}/>
}

export default connect(mapStateToProps)(RoleRoute)