import axios from "axios";
import AuthService from "./AuthServise";
import config from "../config";

let http = axios.create({
    timeout: 60000,
});

http.interceptors.request.use(function (config) {
    if (localStorage.getItem("Authentication") === "true") {
        config.headers.Authorization = "Bearer " + localStorage.getItem("JWT");
    }
    return config;
});

http.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        if (error.response !== undefined) {
            switch (error.response.status) {
                case 401:
                    sessionStorage.setItem(config.redirectLoginUrl, window.location.href);
                    if (localStorage.getItem("Authentication") === "true") {
                        AuthService.logout();
                    }
                    if (window.location.pathname !== "/login")
                        window.location.replace("/login");
                    Promise.reject(error).then(r => console.log(r));
                    break;
                case 403:
                    if (window.location.pathname !== "/login") {
                        if (document.referrer.slice(0, document.referrer.indexOf("/")) === window.location.hostname) {
                            window.history.back();
                        } else {
                            window.location.replace("/main");
                        }
                    }
                    break;
                default:
                    console.log(error);
            }
        }
        return Promise.reject(error);
    }
);

export default http;