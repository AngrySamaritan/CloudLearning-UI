import * as React from "react";
import {Redirect, Route} from "react-router-dom";
import config from "../config";

export default function AuthenticatedRoute(props) {

    if (localStorage.getItem("Authentication") !== "true") {
        sessionStorage.setItem(config.redirectLoginUrl, window.location.pathname);
        return (
            <Redirect to="/login"/>
        );
    } else {
        return <Route {...props}/>
    }
}