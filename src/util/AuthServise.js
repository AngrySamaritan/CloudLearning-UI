import http from "./Http";
import config from "../config";


export default class AuthService {

    static authRequest(username, password, errorSetter) {


        try {
            http.post("/login", null, {
                headers:
                    {
                        Authorization: this.createBasicAuthToken(username, password),
                    },
                responseType: "text"
            }).then((response) => {
                if (response.status === 200) {
                    sessionStorage.removeItem(config.redirectLoginUrl);
                    localStorage.setItem("Authentication", "true");
                    window.location.replace("/main");
                }
            }).catch((error) => {
                if (error.response === undefined) {
                    errorSetter("Something goes wrong. Try again few minutes later");
                } else if (error.response.status === 401) {
                    errorSetter("Wrong username or password");
                } else if (error.response.status === 403) {
                    errorSetter("Please confirm your email before login");
                } else {
                    errorSetter("Something goes wrong. Try again few minutes later");
                }
            });
        } catch (e) {
            console.log(e);
            errorSetter("Something goes wrong. Try again few minutes later");
        }
    }

    static createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    static logout() {
        console.log("LOGOUT");
        localStorage.removeItem("Authentication");
        http.post("/log_out").then(() => {
            window.location.replace("/");
        }).catch((e) => alert(e));
    }


}
