export function hasRole({roles}, role) {
    return roles.some(r => r.toString() === role);
}