import * as React from "react";
import {Switch} from "react-router-dom";
import {ResultComponent} from "../result/ResultComponent";
import AccessManagement from "../components/access-management/AccessManagement";
import TestEdit from "../components/test-edit/TestEdit";
import AuthenticatedRoute from "../util/AuthenticatedRoute";
import TestInfo from "../components/test-info/TestInfo";
import TestCompleting from "../components/test-completing/TestCompleting";

export class TestRouter extends React.Component {
    render() {
        return (
            <div>
                <Switch>
                    <AuthenticatedRoute exact path="/test/create/:subjectId" component={TestEdit}/>
                    <AuthenticatedRoute exact path="/test/:id/edit" component={TestEdit}/>
                    <AuthenticatedRoute exact path="/test/:id" component={TestCompleting}/>
                    <AuthenticatedRoute exact path="/test/:id/info" component={TestInfo}/>
                    <AuthenticatedRoute exact path="/test/result/id:id" component={ResultComponent}/>
                    <AuthenticatedRoute exact path="/test/id:id/management" component={AccessManagement}/>
                </Switch>
            </div>
        );
    }
}