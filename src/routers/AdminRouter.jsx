import {Switch} from "react-router-dom";
import * as React from "react";
import {keywords} from "../config/KeyWords";
import RoleRoute from "../util/RoleRoute";
import SubjectManagement from "../components/subject-management/SubjectManagement";

const AdminRouter = () => {
    return (
        <div>
            <Switch>
                <RoleRoute role={keywords.ROLE_ADMIN} path={"/admin/subjectsManagement"} component={SubjectManagement}/>
            </Switch>
        </div>
    );
}

export default AdminRouter;