import * as React from 'react';
import styles from '../login/login.module.css';
import cx from 'classnames';
import {Button, Card, CardActions, CardContent, Grid, makeStyles, TextField, Typography,} from '@material-ui/core';
import VelocityComponent from 'velocity-react/velocity-component';
import globalStyles from '../style.module.css';

import {Link, useHistory} from 'react-router-dom';
import config from "../../config";
import http from "../../util/Http";
import {NotificationManager} from "react-notifications";


const useStyles = makeStyles((theme) => ({
    inputColor: {
        color: 'white',
    },
    inputLabal2: {
        color: 'white',
        opacity: '0.9',
        fontFamily: 'Arial',
        fontSize: '1.5rem'
    },
    inputLabal: {
        color: 'white',
        opacity: '0.9',
    },
    control: {
        padding: theme.spacing(3),
    },
    button: {
        background: 'transparent',
        border: '#3f51b5 solid 1px',
        '&:hover': {
            background: 'primary',
        },
    },
    cardContent: {
        marginRight: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            alignItems: 'center',
            justify: 'center',
        },
        paddingBottom: theme.spacing(1),
    },
    cardContent2: {
        marginLeft: theme.spacing(6)
    },
    loginLogo: {
        marginTop: theme.spacing(-95),
        position: 'absolute',
    },
    loginCard: {
        background: 'black',
        paddingBottom: theme.spacing(2),
    },

    cardAction: {
        paddingTop: theme.spacing(3),
        justifyContent: 'center',
    },
    inputField: {
        marginRight: theme.spacing(2),
    },
    cardButton: {
        color: '#fff',
    },
    loginToSignup: {
        paddingTop: theme.spacing(1),

        position: 'relative',
        color: '#fff',
        textJustify: 'center',
    },
    signUpLink: {
        color: 'light blue',
    },
    restrorePassword: {
        paddingTop: theme.spacing(1),
        position: 'relative',
        color: '#fff',
    },
}));

class PasswordReset extends React.Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {

            errors: {
                existUser: false,
            },

            serverError: '',
            usernameOrEmail: '',
            rememberMe: false,
            error: '',
            usernameOrEmailError: '',
            usernameOrEmailFlag: false,
        };
        this.PasswordResetContent = this.PasswordResetContent.bind(this);
    }

    handleChange(e) {
        if (e.target.name === 'usernameOrEmail') {
            this.setState({usernameOrEmailFlag: true});
            if (
                this.state.usernameOrEmail.length
            ) {
                this.setState({usernameOrEmailError: ''});
            }
        }
        this.setState({[e.target.name]: e.target.value});
    }

    validate() {
        const errors = {
            usernameOrEmailError: false
        };
        if (this.state.usernameOrEmail.length === 0) {
            this.setState({usernameOrEmailError: 'Username or e-mail is required'});
            errors.usernameOrEmailError = true;
        } else {
            this.setState({usernameOrEmailError: ''});
        }
        return !(errors.usernameOrEmailError);
    }

    errorSetter(error) {
        let state = this.state;
        state.serverError = error;

        if (error === '') {
            if (sessionStorage.getItem(config.redirectLoginUrl)) {
                this.props.history.back();
            } else {
                this.props.history.push("/main");
            }
        } else {
            state.errors.existUser = true
        }
        this.setState(state);
    }

    submit(event) {
        event.preventDefault();

        const isValid = this.validate();

        this.setState({usernameOrEmailFlag: true});

        let fd = new FormData();

        fd.append('account', this.state.usernameOrEmail);
        fd.append('confirmationUrl', config.passwordRestoreUrl);

        if (isValid) {
            http.post("/password/change", fd).then(
                () => {
                    this.props.history.push("/login");
                    NotificationManager.info("Recovery letter was sent to your email", "Check your email", 6000);
                }
            )
        }
    }


    LoginOverlay() {
        return (
            <VelocityComponent
                animation={{
                    opacity: '1',
                    translateX: '100%',
                }}
                runOnMount={true}
                duration={1000}
            >
                <div className={styles.loginOverlay}>
                    <div className={cx(styles.loginOverlay2, 'skew-part')}>
                        <div className={globalStyles.stars}/>
                        <div className={globalStyles.stars2}/>
                        <div className={globalStyles.stars3}/>
                    </div>
                </div>
            </VelocityComponent>
        );
    }

    PasswordResetContent() {
        const classes = useStyles();
        return (
            <VelocityComponent
                animation={{
                    opacity: '4',
                }}
                runOnMount={true}
                delay={1000}
                duration={1000}
            >
                <Grid
                    zeroMinWidth={780}
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{minHeight: '100vh'}}
                >
                    <Grid align="Center" className={classes.loginLogo}>
                        <a href="/">
                            <img src={process.env.PUBLIC_URL + '/img/logo.png'} alt="logo"/>
                        </a>
                    </Grid>
                    <Grid item xs={3}>
                        <Card className={classes.loginCard} elevation={10}>
                            <CardContent className={classes.cardContent}>}
                                <TextField
                                    name="usernameOrEmail"
                                    autoFocus="false"
                                    className={classes.inputField}
                                    size="small"
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.usernameOrEmailFlag &&
                                            this.state.usernameOrEmail.length === 0) ||
                                        this.state.usernameOrEmailError
                                            ? 'Username or e-mail is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.usernameOrEmailError
                                    }
                                    value={this.state.usernameOrEmail}
                                    onChange={this.handleChange.bind(this)}
                                    label="E-MAIL OR USERNAME"
                                    required
                                    fullWidth
                                />


                            </CardContent>
                            <CardActions className={classes.cardAction}>
                                <Button
                                    className={classes.cardButton}
                                    size="large"
                                    variant="outlined"
                                    color="primary"
                                    onClick={(event) => this.submit(event)}
                                >
                                    PASSWORD RESET
                                </Button>
                            </CardActions>
                            <Grid align="Center" className={classes.loginToSignup}>
                                <Typography>
                                    <Link to={"/login"} className={classes.signUpLink}>
                                        {' '}
                                        Cancel
                                    </Link>
                                </Typography>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </VelocityComponent>
        );
    }

    render() {
        return (
            <div className={styles.loginBody}>
                <this.LoginOverlay/>
                <this.PasswordResetContent/>
                {/*<this.NewPassword/>*/}
            </div>
        );
    }
}

export default PasswordReset;
