import * as React from 'react';
import styles from '../login/login.module.css';
import cx from 'classnames';
import {Button, Card, CardActions, CardContent, Grid, makeStyles, TextField, Typography,} from '@material-ui/core';
import VelocityComponent from 'velocity-react/velocity-component';
import globalStyles from '../style.module.css';

import {Link} from 'react-router-dom';
import AuthService from '../../util/AuthServise';
import config from "../../config";
import http from "../../util/Http";


const useStyles = makeStyles((theme) => ({
    inputColor: {
        color: 'white',
    },
    inputLabal: {
        color: 'white',
        opacity: '0.9',
    },
    control: {
        padding: theme.spacing(3),
    },
    button: {
        background: 'transparent',
        border: '#3f51b5 solid 1px',
        '&:hover': {
            background: 'primary',
        },
    },
    cardContent: {
        marginRight: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            alignItems: 'center',
            justify: 'center',
        },
        paddingBottom: theme.spacing(0),
    },
    loginLogo: {
        marginTop: theme.spacing(-95),
        position: 'absolute',
    },
    loginCard: {
        background: 'black',
        paddingBottom: theme.spacing(2),
    },

    cardAction: {
        paddingTop: theme.spacing(3),
        justifyContent: 'center',
    },
    inputField: {
        marginRight: theme.spacing(2),
    },
    cardButton: {
        color: '#fff',
    },
    loginToSignup: {
        paddingTop: theme.spacing(1),

        position: 'relative',
        color: '#fff',
        textJustify: 'center',
    },
    signUpLink: {
        color: 'light blue',
    },
    restrorePassword: {
        paddingTop: theme.spacing(1),
        position: 'relative',
        color: '#fff',
    },
}));

class NewPassword extends React.Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {
            username: "LOADING",
            errors: {
                easyPassword: false,
            },

            serverError: '',
            password: '',
            passwordConfirmation: '',
            error: '',
            passwordError: '',
            passwordConfirmationError: '',
            passwordConfirmationFlag: false,
            passwordFlag: false,
        };
        this.NewPasswordContent = this.NewPasswordContent.bind(this);
    }


    componentDidMount() {
        console.log(this.props.match.params);
        http.get("/password/checkCode/" + this.props.match.params.uid)
            .then(
                (response) => {
                    this.setState(state => ({...state, username: response.data}));
                }
            )
            .catch(
                (error) => {
                    console.error(error);
                    if (error.response.status === 400) {
                        this.props.history.push("/home");
                    }
                }
            )
    }

    handleChange(e) {
        if (e.target.name === 'password') {
            this.setState({passwordFlag: true});
            if (this.state.password.length !== 0) {
                this.setState({passwordError: ''});
            }
        }
        if (e.target.name === 'passwordConfirmation') {
            this.setState({passwordConfirmationFlag: true});
            if (this.state.passwordConfirmation.length !== 0) {
                this.setState({passwordConfirmationError: ''});
            }
        }
        this.setState({[e.target.name]: e.target.value});
    }

    validate() {
        const PasRegEx = /^(?=.{5,20}$)[^а-яёА-ЯЁа-зй-шы-яА-ЗЙ-ШЫІіЎў]*$/;

        const errors = {
            passwordConfirmationError: false,
            passwordError: false,
        };
        if (this.state.password.length === 0) {
            this.setState({passwordError: 'Password is required'});
            errors.passwordError = true;
        } else {
            this.setState({passwordError: ''});
        }
        if (!this.state.password.match(PasRegEx)) {
            this.setState({passwordError: 'Wrong password'});
            errors.passwordError = true;
        } else {
            this.setState({passwordError: ''})
        }
        if (!this.state.passwordConfirmation.match(this.state.password)) {
            this.setState({
                passwordConfirmationError: "Passwords doesn't match",
            });
            errors.passwordConfirmationError = true;
        } else {
            this.setState({passwordConfirmationError: ''});
        }
        if (this.state.passwordConfirmation.length === 0) {
            this.setState({
                passwordConfirmationError: 'Confirmation password is required ',
            });
            errors.passwordConfirmationError = true;
        }

        return !(errors.passwordConfirmationError ||
            errors.passwordError);

    }

    errorSetter(error) {
        let state = this.state;
        state.serverError = error;

        if (error === '') {
            if (sessionStorage.getItem(config.redirectLoginUrl)) {
                this.props.history.back();
            } else {
                this.props.history.push("/main");
            }
        } else {
            state.errors.easyPassword = true
        }
        this.setState(state);
    }

    submit(event) {
        event.preventDefault();

        const isValid = this.validate();

        this.setState({passwordConfirmationFlag: true});
        this.setState({passwordFlag: true});

        if (isValid) {
            http.patch("/password/change/" + this.props.match.params.uid, {
                password: this.state.password,
                passwordConfirmation: this.state.passwordConfirmation,
            }).then(
                () => {
                    AuthService.authRequest(
                        this.state.username,
                        this.state.password,
                        this.errorSetter.bind(this),
                    );
                }
            )

        } else {
            console.log('Oooops');
        }
    }


    LoginOverlay() {
        return (
            <VelocityComponent
                animation={{
                    opacity: '1',
                    translateX: '100%',
                }}
                runOnMount={true}
                duration={1000}
            >
                <div className={styles.loginOverlay}>
                    <div className={cx(styles.loginOverlay2, 'skew-part')}>
                        <div className={globalStyles.stars}/>
                        <div className={globalStyles.stars2}/>
                        <div className={globalStyles.stars3}/>
                    </div>
                </div>
            </VelocityComponent>
        );
    }

    NewPasswordContent() {
        const classes = useStyles();
        return (
            <VelocityComponent
                animation={{
                    opacity: '4',
                }}
                runOnMount={true}
                delay={1000}
                duration={1000}
            >
                <Grid
                    zeroMinWidth={780}
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{minHeight: '100vh'}}
                >
                    <Grid align="Center" className={classes.loginLogo}>
                        <a href="/">
                            <img src={process.env.PUBLIC_URL + '/img/logo.png'} alt="logo"/>
                        </a>
                    </Grid>
                    <Grid item xs={3}>
                        <Card className={classes.loginCard} elevation={10}>
                            <CardContent className={classes.cardContent}>}

                                <TextField
                                    name="password"
                                    autoFocus="false"
                                    className={classes.inputField}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    size="small"
                                    variant="outlined"
                                    error={
                                        (this.state.passwordFlag &&
                                            this.state.password.length === 0) ||
                                        this.state.passwordError ||
                                        this.state.errors.passwordEasy
                                            ? 'Password is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.existUser
                                            ? this.state.serverError
                                            : this.state.passwordError
                                    }
                                    value={this.state.password}
                                    onChange={this.handleChange.bind(this)}
                                    label="NEW PASSWORD"
                                    type="password"
                                    required
                                    fullWidth
                                />


                                <TextField
                                    name="passwordConfirmation"
                                    autoFocus="false"
                                    className={classes.inputField}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    size="small"
                                    variant="outlined"
                                    error={
                                        (this.state.passwordConfirmationFlag &&
                                            this.state.passwordConfirmation.length === 0) ||
                                        this.state.passwordConfirmationError ||
                                        this.state.errors.passwordEasy
                                            ? 'Password is very easy'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.passwordEasy
                                            ? this.state.serverError
                                            : this.state.passwordConfirmationError
                                    }
                                    value={this.state.passwordConfirmation}
                                    onChange={this.handleChange.bind(this)}
                                    label="CONFIRM NEW PASSWORD"
                                    type="password"
                                    required
                                    fullWidth
                                />
                            </CardContent>
                            <CardActions className={classes.cardAction}>
                                <Button
                                    className={classes.cardButton}
                                    size="large"
                                    variant="outlined"
                                    color="primary"
                                    onClick={(event) => this.submit(event)}
                                >
                                    NEW PASSWORD
                                </Button>
                            </CardActions>
                            <Grid align="Center" className={classes.loginToSignup}>
                                <Typography>
                                    <Link to={"/login"} className={classes.signUpLink}>
                                        {' '}
                                        Log in
                                    </Link>
                                </Typography>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </VelocityComponent>
        );
    }

    render() {
        return (
            <div className={styles.loginBody}>
                <this.LoginOverlay/>
                <this.NewPasswordContent/>
            </div>
        );
    }
}

export default NewPassword;
