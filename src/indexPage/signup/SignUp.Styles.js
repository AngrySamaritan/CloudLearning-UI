import styled from 'styled-components';
import {Link} from "react-router-dom";

export const ConfirmText = styled.h1`
  text-align: center;
  padding-top: 40px;
  color: #adb5bd;
  font-family: Montserrat, "Helvetica Neue", "Lucida Grande", Arial, Verdana, sans-serif;
  font-size: xx-large;
`

export const ConfirmEmail = styled.h2`
  text-align: center;
  padding-top: 40px;
  color: #adb5bd;
  font-family: Montserrat, "Helvetica Neue", "Lucida Grande", Arial, Verdana, sans-serif;
  font-size: xx-large;
  text-overflow: ellipsis;
`

export const StyledLink = styled(Link)`
  position: absolute;
  bottom: 10%;
  width: 100%;
  display: flex;
  justify-content: center;
  
`

export const StyledButton = styled.button`
  width: 100px;
  height: 50px;
  background: transparent;
  border-radius: 5px;
  color: white;
  font-family: Montserrat, "Helvetica Neue", "Lucida Grande", Arial, Verdana, sans-serif;
  font-size: larger;
  border: 1px solid #FFFFFF;
  transition: linear 0.3s;  
  &:hover {
    background: #4a90e2;
    cursor: pointer;
  }
`