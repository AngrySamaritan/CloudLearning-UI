import * as React from 'react';

import styles from './sign_up.module.css';
import globalStyles from '../style.module.css';
import cx from 'classnames';
import VelocityComponent from 'velocity-react/velocity-component';
import http from '../../util/Http';
import {Stage} from '../../config/stages';
import {Link} from 'react-router-dom';
import config from '../../config';
import {Button, Card, CardActions, CardContent, Grid, makeStyles, TextField, Typography,} from '@material-ui/core';
import {ConfirmEmail, ConfirmText, StyledButton, StyledLink} from "./SignUp.Styles";

const useStyles = makeStyles((theme) => ({
    signupLogo: {
        marginTop: theme.spacing(-95),
        position: 'absolute',
    },
    inputField: {
        color: 'white',
    },
    inputColor: {
        color: 'white',
    },
    inputLabal: {
        color: 'white',
        opacity: '0.9',
    },
    signupCard: {
        background: 'black',
        paddingBottom: theme.spacing(2),
    },
    cardContent: {
        marginRight: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            alignItems: 'center',
            justify: 'center',
        },
        paddingBottom: theme.spacing(0),
    },

    cardAction: {
        justifyContent: 'center',
        paddingTop: theme.spacing(0.3),
    },
    cardButton: {
        color: '#fff',
    },
    signupToLogin: {
        position: 'relative',
        color: '#fff',
        textJustify: 'center',
    },
    loginLink: {
        color: 'light blue',
    },
    Typography: {
        color: 'white',
    },
}));

class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.Content = this.Content.bind(this);
        this.SignUpButton = this.SignUpButton.bind(this);
        this.UsernameError = this.UsernameError.bind(this);
        this.EmailError = this.EmailError.bind(this);
        this.PasswordMatchError = this.PasswordMatchError.bind(this);
        this.MailConfirmation = this.MailConfirmation.bind(this);
        this.PasswordEasyError = this.PasswordEasyError.bind(this);
        this.state = {
            stage: Stage.SIGN_UP.FORM,
            errors: {
                usernameExist: false,
                emailExist: false,
                passwordEasy: false,
                emailInvalid: false,
                passwordsNotMatches: false,
            },
            group: '',
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            passwordConfirmation: '',
            confirmationUrl: config.confirmationUrl,

            usernameError: '',
            firstNameError: '',
            lastNameError: '',
            emailError: '',
            passwordError: '',
            passwordConfirmationError: '',
            usernameFlag: false,
            firstNameFlag: false,
            lastNameFlag: false,
            emailFlag: false,
            passwordFlag: false,
            passwordConfirmationFlag: false,
            groupFlag: false,
        };
    }

    Overlay() {
        return (
            <VelocityComponent
                animation={{
                    translateX: '100%',
                    opacity: '1',
                }}
                duration={1000}
                runOnMount={true}
            >
                <div className={cx(styles.signUpOverlay2, 'skew-part')}>
                    <div className={globalStyles.stars}/>
                    <div className={globalStyles.stars2}/>
                    <div className={globalStyles.stars3}/>
                </div>
            </VelocityComponent>
        );
    }

    handleChange(e) {
        const usernameRegEx = /^(?=.{8,20}$)(?![_.])[a-zA-Z0-9._-]+(?<![_.])$/;
        const nameRegEx = /^[a-zA-Zа-яёА-ЯЁа-зй-шы-яЁА-ЗЙ-ШЫІіЎў]*$/;
        const emailRegEx = /^[a-zA-Zа-яёА-ЯЁа-зй-шы-яЁА-ЗЙ-ШЫІіЎў0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        const groupRegEx = /^(?=.{5,20})(?![_.])[0-9a-zA-Zа-яёА-ЯЁёа-зй-шы-яЁА-ЗЙ-ШЫІіЎў._#-]+(?<![_.])$/;

        if (e.target.name === 'username') {
            this.setState({usernameFlag: true});
            if (
                this.state.username.length !== 0 &&
                this.state.username.match(usernameRegEx)
            ) {
                this.setState({usernameError: ''});
            }
        }
        if (e.target.name === 'group') {
            this.setState({groupFlag: true});
            if (this.state.group.length !== 0 && this.state.group.match(groupRegEx)) {
                this.setState({groupError: ''});
            }
        }
        if (e.target.name === 'firstName') {
            this.setState({firstNameFlag: true});
            if (
                this.state.firstName.length !== 0 &&
                this.state.firstName.match(nameRegEx)
            ) {
                this.setState({firstNameError: ''});
            }
        }
        if (e.target.name === 'lastName') {
            this.setState({lastNameFlag: true});
            if (
                this.state.lastName.length !== 0 &&
                this.state.lastName.match(nameRegEx)
            ) {
                this.setState({lastNameError: ''});
            }
        }
        if (e.target.name === 'email') {
            this.setState({emailFlag: true});
            if (this.state.email.length !== 0 && this.state.email.match(emailRegEx)) {
                this.setState({emailError: ''});
            }
        }
        if (e.target.name === 'password') {
            this.setState({passwordFlag: true});
            if (this.state.password.length !== 0) {
                this.setState({passwordError: ''});
            }
        }
        if (e.target.name === 'passwordConfirmation') {
            this.setState({passwordConfirmationFlag: true});
            if (this.state.passwordConfirmation.length !== 0) {
                this.setState({passwordConfirmationError: ''});
            }
        }
        this.setState({[e.target.name]: e.target.value});
    }

    validate() {
        const usernameRegEx = /^(?=.{5,20}$)(?![_.])[a-zA-Z0-9._-]+(?<![_.])$/;
        const nameRegEx = /^[a-zA-Zа-яёА-ЯЁа-зй-шы-яЁА-ЗЙ-ШЫІіЎў]*$/;
        const emailRegEx = /^[a-zA-Zа-яёА-ЯЁа-зй-шы-яЁА-ЗЙ-ШЫІіЎў0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        const groupRegEx = /^(?=.{5,20})(?![_.])[0-9a-zA-Zа-яёА-ЯЁёа-зй-шы-яЁА-ЗЙ-ШЫІіЎў._#-]+(?<![_.])$/;
        const errors = {
            usernameError: false,
            firstNameError: false,
            lastNameError: false,
            emailError: false,
            passwordError: false,
            passwordConfirmationError: false,
        };
        if (!this.state.username.match(usernameRegEx)) {
            this.setState({usernameError: 'Invalid username'});
            errors.usernameError = true;
        } else {
            this.setState({usernameError: ''});
        }
        if (this.state.username.length === 0) {
            this.setState({usernameError: 'Username field is required'});
            errors.usernameError = true;
        }
        if (!this.state.group.match(groupRegEx)) {
            this.setState({groupError: 'Invalid group'});
            errors.groupError = true;
        } else {
            this.setState({groupError: ''});
        }
        if (this.state.group.length === 0) {
            this.setState({groupError: 'Group field is required'});
            errors.groupError = true;
        }

        if (this.state.firstName.length === 0) {
            this.setState({firstNameError: 'First Name field is required'});
            errors.firstNameError = true;
        } else {
            this.setState({firstNameError: ''});
        }
        if (!this.state.firstName.match(nameRegEx)) {
            this.setState({firstNameError: 'Invalid First Name'});
            errors.firstNameError = true;
        }
        if (this.state.lastName.length === 0) {
            this.setState({lastNameError: 'Last Name is required'});
            errors.lastNameError = true;
        } else {
            this.setState({lastNameError: ''});
        }
        if (!this.state.lastName.match(nameRegEx)) {
            this.setState({lastNameError: 'Invalid Last Name'});
            errors.firstNameError = true;
        }
        if (!this.state.email.match(emailRegEx)) {
            this.setState({emailError: 'Invalid type of email'});
            errors.emailError = true;
        } else {
            this.setState({emailError: ''});
        }
        if (this.state.email.length === 0) {
            this.setState({emailError: 'Email is required'});
            errors.emailError = true;
        }

        if (this.state.password.length === 0) {
            this.setState({passwordError: 'password field is required'});
            errors.passwordError = true;
        } else {
            this.setState({passwordError: ''});
        }

        if (!this.state.passwordConfirmation.match(this.state.password)) {
            this.setState({
                passwordConfirmationError: "Passwords doesn't match",
            });
            errors.passwordConfirmationError = true;
        } else {
            this.setState({passwordConfirmationError: ''});
        }
        if (this.state.passwordConfirmation.length === 0) {
            this.setState({
                passwordConfirmationError: 'Confirmation is required ',
            });
            errors.passwordConfirmationError = true;
        }
        if (
            errors.usernameError ||
            errors.firstNameError ||
            errors.lastNameError ||
            errors.emailError ||
            errors.passwordError ||
            errors.passwordConfirmationError
        ) {
            return false;
        }

        return true;
    }

    submit() {
        let state = this.state;
        state.stage = Stage.LOADING;
        const user = {
            username: this.state.username,
            group: this.state.group,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            password: this.state.password,
            passwordConfirmation: this.state.passwordConfirmation,
            confirmationUrl: config.confirmationUrl,
        };
        console.log(user);
        this.setState(state);
        const isValid = this.validate();

        if (isValid) {
            http
                .post('/sign_up', user)
                .then((response) => {
                    this.setState((state) => {
                        if (response.status === 200) {
                            state.stage = Stage.SIGN_UP.SUCCESS;
                            return (
                                <div className={styles.confirmationForm}>
                                    <confirm>A confirmation code has been sent to the mail</confirm>
                                    <mail>{this.state.email}</mail>
                                    <h1>
                                        <Link to={'/login'}>
                                            <button className={styles.ok}>Okay</button>
                                        </Link>
                                    </h1>
                                </div>
                            )
                        }
                    });
                })
                .catch((exception) => {
                    let response = exception.response;
                    this.setState((state) => {
                        if (response.status === 400) {
                            console.log(response.data);
                            if (response.data.errors.username) {
                                this.setState({
                                    usernameError: response.data.errors.username,
                                });
                            }
                            if (response.data.errors.email) {
                                this.setState({
                                    emailError: response.data.errors.email,
                                });
                            }
                            if (response.data.errors.password) {
                                this.setState({
                                    passwordError: response.data.errors.password,
                                });
                            }
                            state.errors = response.data.errors;
                        } else {
                            console.log('Ops! We have a problem', response);
                        }
                        state.stage = Stage.SIGN_UP.FORM;
                        return state;
                    });
                });
        } else {
            console.log('faggot');
        }
    }

    UsernameError() {
        if (this.state.errors.username) {
            return (
                <div id="username_exist_error" className={styles.errorForm}>
                    <label className={cx(styles.input, styles.usernameError)}>
                        {this.state.errors.username}
                    </label>
                </div>
            );
        } else {
            return <div/>;
        }
    }

    EmailError() {
        if (this.state.errors.email) {
            return (
                <div className={styles.errorForm}>
                    <label className={cx(styles.input, styles.emailError)}>
                        {this.state.errors.email}
                    </label>
                </div>
            );
        } else {
            return <div/>;
        }
    }

    PasswordMatchError() {
        if (this.state.errors.passwordMatches) {
            return (
                <div id="password_matches_error" className={styles.errorForm}>
                    <label className={cx('text', styles.passwordMatchError)}>
                        {this.state.errors.passwordMatches}
                    </label>
                </div>
            );
        } else {
            return <div/>;
        }
    }

    PasswordEasyError() {
        if (this.state.errors.password) {
            return (
                <div className={styles.errorForm}>
                    <label className={cx('text', styles.passwordEasyError)}>
                        {this.state.errors.password}
                    </label>
                </div>
            );
        } else {
            return <div/>;
        }
    }

    Content() {
        const classes = useStyles();
        return (
            <VelocityComponent
                animation={{
                    opacity: '1',
                }}
                runOnMount={true}
                duration={1000}
                delay={1000}
            >
                {/*<section style={{opacity: 0}}>*/}
                <Grid
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{minHeight: '100vh', minWidth: '768px'}}
                >

                    <Grid align="Center" className={classes.signupLogo}>
                        <a href="/">
                            <img src={process.env.PUBLIC_URL + '/img/logo.png'} alt="logo"/>
                        </a>
                    </Grid>
                    <Grid item xs={3} style={{minWidth: '600px'}}>
                        <Card className={classes.signupCard} elevation={10}>
                            <CardContent className={classes.cardContent}>
                                <TextField
                                    name="username"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.usernameFlag &&
                                            this.state.username.length === 0) ||
                                        this.state.usernameError
                                            ? 'username field is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.usernameExist
                                            ? 'Username already exist'
                                            : this.state.usernameError
                                    }
                                    value={this.state.username}
                                    label="USERNAME"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="group"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.groupFlag && this.state.group.length === 0) ||
                                        this.state.groupError
                                            ? 'group field is required'
                                            : ''
                                    }
                                    helperText={this.state.groupError}
                                    value={this.state.group}
                                    label="GROUP"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="firstName"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.firstNameFlag &&
                                            this.state.firstName.length === 0) ||
                                        this.state.firstNameError
                                            ? 'firstname field is required'
                                            : ''
                                    }
                                    helperText={this.state.firstNameError}
                                    value={this.state.firstName}
                                    label="FIRSTNAME"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="lastName"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.lastNameFlag &&
                                            this.state.lastName.length === 0) ||
                                        this.state.lastNameError
                                            ? 'lastname field is required'
                                            : ''
                                    }
                                    helperText={this.state.lastNameError}
                                    value={this.state.lastName}
                                    label="LASTNAME"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="email"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.emailFlag && this.state.email.length === 0) ||
                                        this.state.emailError
                                            ? 'email field is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.emailExist
                                            ? 'Email already exist'
                                            : this.state.emailError
                                    }
                                    value={this.state.email}
                                    label="EMAIL"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="password"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.passwordFlag &&
                                            this.state.password.length === 0) ||
                                        this.state.passwordError
                                            ? 'Password field is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.passwordEasy
                                            ? 'Invalid password'
                                            : this.state.passwordError
                                    }
                                    value={this.state.password}
                                    label="PASSWORD"
                                    type="password"
                                    required
                                    fullWidth
                                />
                                <TextField
                                    name="passwordConfirmation"
                                    autoFocus={false}
                                    className={classes.inputField}
                                    style={{marginTop: '0px'}}
                                    size="small"
                                    onChange={this.handleChange.bind(this)}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.passwordConfirmationFlag &&
                                            this.state.passwordConfirmation.length === 0) ||
                                        this.state.passwordConfirmationError
                                            ? 'passwordConfirmation field is required'
                                            : ''
                                    }
                                    helperText={this.state.passwordConfirmationError}
                                    value={this.state.passwordConfirmation}
                                    label="CONFIRM PASSWORD"
                                    type="password"
                                    required
                                    fullWidth
                                />
                            </CardContent>
                            <CardActions className={classes.cardAction}>
                                <Button
                                    className={classes.cardButton}
                                    size="large"
                                    variant="outlined"
                                    color="primary"
                                    onClick={(event) => this.submit(event)}
                                >
                                    Sign Up
                                </Button>
                            </CardActions>
                            <Grid align="Center" className={classes.signupToLogin}>
                                <Typography>
                                    Already have an account?
                                    <a href="/login" className={classes.signUpLink}>
                                        {' '}
                                        Log in.
                                    </a>
                                </Typography>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </VelocityComponent>
        );
    }

    SignUpButton() {
        if (
            this.state.user.username.length === 0 ||
            this.state.user.firstName.length === 0 ||
            this.state.user.lastName.length === 0 ||
            this.state.user.email.length === 0 ||
            this.state.user.password.length === 0 ||
            this.state.user.passwordConfirmation.length === 0 ||
            this.state.stage === Stage.LOADING
        ) {
            return (
                <h1>
                    <button
                        type="button"
                        className={styles.disabledSignUp}
                        disabled={true}
                    >
                        Sign up
                    </button>
                </h1>
            );
        } else {
            return (
                <h1>
                    <button type="button" className={styles.signUp} onClick={this.submit}>
                        Sign up
                    </button>
                </h1>
            );
        }
    }

    MailConfirmation() {
        return (
            <div className={styles.confirmationForm}>
                <ConfirmText>A confirmation code has been sent to the mail: </ConfirmText>
                <ConfirmEmail>{this.state.email}</ConfirmEmail>
                <StyledLink to={'/login'}>
                    <StyledButton>Okay</StyledButton>
                </StyledLink>
            </div>

        );
    }

    render() {
        switch (this.state.stage) {
            case Stage.LOADING:
            case Stage.SIGN_UP.FORM:
                return (
                    <div className={styles.signUpBody}>
                        <this.Overlay/>
                        <this.Content/>
                    </div>
                );
            case Stage.SIGN_UP.SUCCESS:
                return (
                    <div className={styles.signUpBody}>
                        <this.Overlay/>
                        <this.MailConfirmation/>
                    </div>
                );
            default:
                return <div>ERROR</div>;
        }
    }
}

export default SignUp;
