import * as React from 'react';
import styles from './login.module.css';
import cx from 'classnames';
import {Button, Card, CardActions, CardContent, Grid, makeStyles, TextField, Typography,} from '@material-ui/core';
import VelocityComponent from 'velocity-react/velocity-component';
import globalStyles from '../style.module.css';

import {Link} from 'react-router-dom';
import AuthService from '../../util/AuthServise';
import config from "../../config";


const useStyles = makeStyles((theme) => ({
    inputColor: {
        color: 'white',
    },
    inputLabal: {
        color: 'white',
        opacity: '0.9',
    },
    control: {
        padding: theme.spacing(3),
    },
    button: {
        background: 'transparent',
        border: '#3f51b5 solid 1px',
        '&:hover': {
            background: 'primary',
        },
    },
    cardContent: {
        marginRight: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(2),
            alignItems: 'center',
            justify: 'center',
        },
        paddingBottom: theme.spacing(0),
    },
    loginLogo: {
        marginTop: theme.spacing(-95),
        position: 'absolute',
    },
    loginCard: {
        background: 'black',
        paddingBottom: theme.spacing(2),
    },

    cardAction: {
        paddingTop: theme.spacing(0),
        justifyContent: 'center',
    },
    inputField: {
        marginRight: theme.spacing(2),
    },
    cardButton: {
        color: '#fff',
    },
    loginToSignup: {
        paddingTop: theme.spacing(1),

        position: 'relative',
        color: '#fff',
        textJustify: 'center',
    },
    signUpLink: {
        color: 'light blue',
    },
    restrorePassword: {
        paddingTop: theme.spacing(1),
        position: 'relative',
        color: '#fff',
    },
}));

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
        this.state = {

            errors: {
                existUser: false,
            },

            serverError: '',
            username: '',
            password: '',
            rememberMe: false,
            error: '',
            usernameError: '',
            passwordError: '',
            usernameFlag: false,
            passwordFlag: false,
        };
        this.LoginContent = this.LoginContent.bind(this);
        this.PasswordRecovery = this.PasswordRecovery.bind(this);
        this.NewPassword = this.NewPassword.bind(this);
    }

    handleChange(e) {
        if (e.target.name === 'username') {
            this.setState({usernameFlag: true});
            if (
                this.state.username.length
            ) {
                this.setState({usernameError: ''});
            }
        }
        if (e.target.name === 'password') {
            this.setState({passwordFlag: true});
            if (
                this.state.password.length !== 0
            ) {
                this.setState({passwordError: ''});
            }
        }
        this.setState({[e.target.name]: e.target.value});
    }

    validate() {
        const PasRegEx = /^(?=.{5,20}$)[^а-яёА-ЯЁа-зй-шы-яА-ЗЙ-ШЫІіЎў]*$/;

        const errors = {
            usernameError: false,
            passwordError: false,
        };
        if (this.state.username.length === 0) {
            this.setState({usernameError: 'Username is required'});
            errors.usernameError = true;
        } else {
            this.setState({usernameError: ''});
        }
        if (this.state.password.length === 0) {
            this.setState({passwordError: 'Password is required'});
            errors.passwordError = true;
        } else {
            this.setState({passwordError: ''});
        }
        if (!this.state.password.match(PasRegEx)) {
            this.setState({passwordError: 'Wrong password or username'});
            errors.passwordError = true;
        } else {
            this.setState({passwordError: ''})
        }

        return !(errors.usernameError ||
            errors.passwordError);

    }

    errorSetter(error) {
        let state = this.state;
        state.serverError = error;

        if (error === '') {
            if (sessionStorage.getItem(config.redirectLoginUrl)) {
                this.props.history.back();
            } else {
                this.props.history.push("/main");
            }
        } else {
            state.errors.existUser = true
        }
        this.setState(state);
    }

    submit(event) {
        event.preventDefault();

        const isValid = this.validate();

        this.setState({usernameFlag: true});
        this.setState({passwordFlag: true});

        if (isValid) {
            console.log(this.state);
            AuthService.authRequest(
                this.state.username,
                this.state.password,
                this.errorSetter.bind(this),
            );
        } else {
            console.log('No valid');
        }
    }


    PasswordRecovery() {
        return (
            <div className={styles.passwordRecoveryForm}>
                <confirm>A recovery code has been sent to the mail</confirm>
                <mail>{this.state.user.email}</mail>
                <h1>
                    <button className={styles.ok}>Okay</button>
                </h1>
            </div>
        );
    }

    NewPassword() {
        return (
            <div className={styles.passwordRecoveryForm}>
                <form action="" className={styles.uiForm}>
                    <div className={styles.textForm}>
                        <input
                            id="password"
                            type="password"
                            className={cx(styles.input, styles.l1)}
                            onChange={(event) => {
                                let state = this.state;
                                state.user.password = event.target.value;
                                this.setState(state);
                            }}
                        />
                        <label className={styles.l1} htmlFor="password">
                            Password
                        </label>
                    </div>

                    <div className={styles.textForm}>
                        <input
                            id="confirmPassword"
                            type="password"
                            className={cx(styles.input, styles.l1)}
                            onChange={(event) => {
                                let state = this.state;
                                state.user.passwordMatches = event.target.value;
                                this.setState(state);
                            }}
                        />
                        <label className={styles.l1} htmlFor="confirmPassword">
                            Confirm password
                        </label>
                    </div>
                </form>

                <div className={styles.position}>
                    <button className={styles.ok}>Okay</button>
                </div>
            </div>
        );
    }

    LoginOverlay() {
        return (
            <VelocityComponent
                animation={{
                    opacity: '1',
                    translateX: '100%',
                }}
                runOnMount={true}
                duration={1000}
            >
                <div className={styles.loginOverlay}>
                    <div className={cx(styles.loginOverlay2, 'skew-part')}>
                        <div className={globalStyles.stars}/>
                        <div className={globalStyles.stars2}/>
                        <div className={globalStyles.stars3}/>
                    </div>
                </div>
            </VelocityComponent>
        );
    }

    LoginContent() {
        const classes = useStyles();
        return (
            <VelocityComponent
                animation={{
                    opacity: '4',
                }}
                runOnMount={true}
                delay={1000}
                duration={1000}
            >
                <Grid
                    zeroMinWidth={780}
                    container
                    spacing={0}
                    direction="column"
                    alignItems="center"
                    justify="center"
                    style={{minHeight: '100vh'}}
                >
                    <Grid align="Center" className={classes.loginLogo}>
                        <a href="/">
                            <img src={process.env.PUBLIC_URL + '/img/logo.png'} alt="logo"/>
                        </a>
                    </Grid>
                    <Grid item xs={3}>
                        <Card className={classes.loginCard} elevation={10}>
                            <CardContent className={classes.cardContent}>}

                                <TextField
                                    name="username"
                                    autoFocus="false"
                                    className={classes.inputField}
                                    size="small"
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    variant="outlined"
                                    error={
                                        (this.state.usernameFlag &&
                                            this.state.username.length === 0) ||
                                        this.state.usernameError
                                            ? 'Username is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.usernameError
                                    }
                                    value={this.state.username}
                                    onChange={this.handleChange.bind(this)}
                                    label="USERNAME"
                                    required
                                    fullWidth
                                />


                                <TextField
                                    name="password"
                                    autoFocus="false"
                                    className={classes.inputField}
                                    InputProps={{className: classes.inputColor}}
                                    InputLabelProps={{className: classes.inputLabal}}
                                    size="small"
                                    variant="outlined"
                                    error={
                                        (this.state.passwordFlag &&
                                            this.state.password.length === 0) ||
                                        this.state.passwordError ||
                                        this.state.errors.existUser
                                            ? 'Password is required'
                                            : ''
                                    }
                                    helperText={
                                        this.state.errors.existUser
                                            ? this.state.serverError
                                            : this.state.passwordError
                                    }
                                    value={this.state.password}
                                    onChange={this.handleChange.bind(this)}
                                    label="PASSWORD"
                                    type="password"
                                    required
                                    fullWidth
                                />
                            </CardContent>
                            <CardActions className={classes.cardAction}>
                                <Button
                                    className={classes.cardButton}
                                    size="large"
                                    variant="outlined"
                                    color="primary"
                                    onClick={(event) => this.submit(event)}
                                >
                                    Log In
                                </Button>
                            </CardActions>
                            <Grid align="Center" className={classes.loginToSignup}>
                                <Typography>
                                    New to Cloud Learning?
                                    <Link to={"/sign_up"} className={classes.signUpLink}>
                                        {' '}
                                        Create an account.
                                    </Link>
                                </Typography>
                            </Grid>
                            <Grid align="Center" className={classes.restrorePassword}>
                                <Typography>
                                    <Link to={"/password_recovery"} className={classes.signUpLink}>
                                        {' '}
                                        Forgot password?
                                    </Link>
                                </Typography>
                            </Grid>
                        </Card>
                    </Grid>
                </Grid>
            </VelocityComponent>
        );
    }

    render() {
        return (
            <div className={styles.loginBody}>
                <this.LoginOverlay/>
                <this.LoginContent/>
                {/*<this.NewPassword/>*/}
            </div>
        );
    }
}

export default Login;
